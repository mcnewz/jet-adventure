#!/bin/bash

SCRIPT_DIR=$(cd $(dirname $0) ; pwd -P)

runs_inside_gocd() {
  test -n "${GO_JOB_NAME}"
}

docker_run() {
    local image_id=${image_id:-$(docker build . -q)}
    if runs_inside_gocd; then
        local args="-v godata:/godata -w $(pwd)"
    else
        local args="-it -v $(pwd):/workspace -w /workspace"
    fi
    DOCKER_ARGS="${DOCKER_ARGS} -v ${HOME}/.aws:/root/.aws"
    docker run --rm --hostname $(hostname) ${args} \
      --env-file <(env | grep AWS_) \
      --env-file <(env | grep JET_) \
      --env-file <(env | grep SPRING_) \
      --env-file <(env | grep SERVER_) \
      ${DOCKER_ARGS} ${image_id} "$@"
}

backend() {
    docker volume inspect jet-adventure-gradle-cache >/dev/null 2>&1 || docker volume create jet-adventure-gradle-cache
    DOCKER_ARGS="${DOCKER_ARGS} -v jet-adventure-gradle-cache:/home/gradle/.gradle" docker_run "$@"
}

goal_start () {
  local DOCKER_ARGS="-p 8082:8082"
  local HOST_IP=$(ifconfig en0 | grep "inet " | awk '{ print $2 }')
  export JET_URL="http://${HOST_IP}:8081"
  export SPRING_PROFILES_ACTIVE="EWI_STUB_ACCESS,IFU_STUB_ACCESS,IDOC_STUB_ACCESS,AWS_SECRET_STUB_ACCESS"
  export SERVER_PORT=8082

  backend gradle clean bootRun

  unset JET_URL
  unset SPRING_PROFILES_ACTIVE
  unset SERVER_PORT
}

goal_build () {
    echo "BUILD"
    backend gradle clean bootJar
}

goal_lint () {
    echo "LINT"
    backend gradle clean ktlint
}

goal_test () {
    echo "TEST"

    if [ -n "${SONARQUBE_TOKEN}" ]; then
    sonar_args="sonarqube -Dsonar.host.url=https://sonarqube.tools.buk0.com -Dsonar.login=${SONARQUBE_TOKEN} -Ddetekt.sonar.kotlin.config.path=./detekt.yml"
    else
      sonar_args=""
    fi

    backend gradle clean test jacocoTestCoverageVerification ${sonar_args}
}

goal_check-dependencies() {
  backend gradle dependencyCheckAnalyze
}
goal_check-secbugs() {
  backend gradle spotbugsMain
}

goal_pre-push() {
    echo "PRE-PUSH"
    backend gradle clean ktlint ktlintFormat test
}

goal_deploy() {
    cd ${SCRIPT_DIR}/infrastructure
    ansible-playbook -i inventory playbook.yml
}

if type -t "goal_$1" &>/dev/null; then
  goal_$1 ${@:2}
else
  echo "usage: $0 <goal>
goal:
    start              -- start application
    build              -- build an executable jar file
    lint               -- lint code
    check-dependencies -- check dependencies for security vulnerabilities
    check-secbugs      -- run static analysis to find common security bugs
    pre-push           -- run pre-push verification
    test               -- run tests
"
fi
