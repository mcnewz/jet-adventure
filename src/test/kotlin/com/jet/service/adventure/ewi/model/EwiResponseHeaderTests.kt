package com.jet.service.adventure.ewi.model

import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.context
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import kotlin.test.assertEquals

class EwiResponseHeaderTests : Spek ({

    describe("EwiResponseHeader") {

        describe("parse") {
            context("valid format") {
                val result = EwiResponseHeader.parse("0000-Success")
                it("return correct header") {
                    assertEquals("0000", result.code)
                }
                it("return correct description") {
                    assertEquals("Success", result.description)
                }
            }
            context("invalid format") {
                val result = EwiResponseHeader.parse("59 . หมายเลขบัตร ไม่ถูกต้อง !!")
                it("return empty header") {
                    assertEquals("", result.code)
                }
                it("return everything as description") {
                    assertEquals("59 . หมายเลขบัตร ไม่ถูกต้อง !!", result.description)
                }
            }
            context("description contains -") {
                val result = EwiResponseHeader.parse("0008-Invalid TH-Title.")
                it("return correct header") {
                    assertEquals("0008", result.code)
                }
                it("return correct description") {
                    assertEquals("Invalid TH-Title.", result.description)
                }
            }
        }
    }
})