package com.jet.service.adventure.ewi.service

import com.jet.service.adventure.ewi.exception.AfsResponseException
import com.jet.service.adventure.ewi.model.CreateLoanApplicationParam
import com.jet.service.adventure.ewi.model.CreateLoanApplicationResult
import com.jet.service.adventure.ewi.model.MarkFlagParam
import com.jet.service.adventure.model.ProductType
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.eq
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.reset
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.context
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class EwiServiceTests : Spek ({

    describe("EwiService") {
        val afsSerializer = AfsSerializer()
        val mockClient = mock<EwiClient>()
        val ewiService = EwiService(mockClient)

        beforeEachTest {
            reset(mockClient)
        }

        describe("createLoanApplication") {

            val param = CreateLoanApplicationParam(
                    "นาย",
                    "th fn ทดสอบ",
                    "th ln ทดสอบ",
                    "",
                    "en fn test",
                    "en ln test",
                    "05102529",
                    "",
                    "1100300324569",
                    "05102566",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "10110",
                    "",
                    "",
                    "45980.00",
                    "New Car",
                    "",
                    "0840941611",
                    "13042561",
                    "090015",
                    "",
                    "",
                    "",
                    "",
                    "1505",
                    "Y",
                    "S1:Y,S2:N,S3:Y,S4:Y,S5:Y,S6:Y",
                    "N",
                    "jet-id",
                    "No--t"
            )
            val successPDataOut = "นาย|th fn ทดสอบ|th ln ทดสอบ||en fn test|en ln test|05102529||1100300324569|05102566||||||10110|||45980.00|New Car||0840941611|13042561|090015|||||1505|Y|S1:Y,S2:N,S3:Y,S4:Y,S5:Y,S6:Y|N||No--t|                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      0000-Success                                                                                                                                                                                            GECAL|HP|14|610410008|123456|R15|KORAT"
            val failPDataOut = "นาย|th fn ทดสอบ|th ln ทดสอบ||en fn test|en ln test|05102529||1100300324569|05102566||||||10110|||45980.00|New Car||0840941611|13042561|090015|||||1505|Y|S1:Y,S2:N,S3:Y,S4:Y,S5:Y,S6:Y|N||No--t|                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      0016-Invalid Maketing Date.                                                                                                                                                                             GECAL|HP|14|610410008|123456|R15|KORAT"

            context("Successfully submit to afs.") {

                var result: CreateLoanApplicationResult? = null
                beforeEachTest {
                    whenever(mockClient.send(any(), any(), any(), any())).thenReturn(successPDataOut)
                    result = ewiService.createLoanApplication(param, ProductType.FOUR_WHEELER)
                }

                it("Pass correct param to client") {
                    verify(mockClient).send(
                            eq("JT1I001"),
                            eq("jet-id"),
                            eq(ProductType.FOUR_WHEELER),
                            eq(afsSerializer.serialize(param)))
                }

                it("Return correct CreateLoanApplicationResult") {
                    assertEquals(CreateLoanApplicationResult("GECAL", "HP", "14", "610410008", "123456", "R15", "KORAT"), result)
                }
            }

            context("AFS return business error") {
                beforeEachTest {
                    whenever(mockClient.send(any(), any(), any(), any())).thenReturn(failPDataOut)
                }

                it("Throw AfsResponseException") {
                    assertFailsWith (AfsResponseException::class) {
                        ewiService.createLoanApplication(param, ProductType.FOUR_WHEELER)
                    }
                }

                it("Has correct exception message") {
                    try {
                        ewiService.createLoanApplication(param, ProductType.FOUR_WHEELER)
                    } catch (e: AfsResponseException) {
                        assertEquals("Invalid Maketing Date.", e.message)
                    }
                }

                it("Has correct exception code") {
                    try {
                        ewiService.createLoanApplication(param, ProductType.FOUR_WHEELER)
                    } catch (e: AfsResponseException) {
                        assertEquals("0016", e.code)
                    }
                }
            }
        }

        describe("markFlag") {

            val markFlagParam = MarkFlagParam(
                "GECAL",
                "MC",
                "12",
                "610648296",
                "A",
                "JT",
                "100100"
            )
            val productType = ProductType.TWO_WHEELER

            context("Successfully markFlag to afs.") {
                val pDataOut = String.format("%1$4950s%2$200s", "", "0000-Success")
                val pDataIn = "GECAL|MC|12|610648296|A|JT|"

                beforeEachTest {
                    whenever(mockClient.send(any(), any(), any(), any())).thenReturn(pDataOut)
                    ewiService.markFlag(markFlagParam, productType)
                }

                it("Pass correct param to client") {
                    verify(mockClient).send(eq("JT1I002"), eq("100100"), eq(productType), eq(pDataIn))
                }
            }

            context("AFS return business error") {
                val pDataOut = String.format("%1$4950s%2$200s", "", "0010-Invalid Mark Flag.")

                beforeEachTest {
                    whenever(mockClient.send(any(), any(), any(), any())).thenReturn(pDataOut)
                }

                it("Throw AfsResponseException") {
                    assertFailsWith (AfsResponseException::class) {
                        ewiService.markFlag(markFlagParam, productType)
                    }
                }
            }
        }
    }
})