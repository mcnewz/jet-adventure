package com.jet.service.adventure.ewi.model

import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.context
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import kotlin.test.assertEquals

class CreateLoanApplicationResponseDataTests : Spek ({

    describe("CreateLoanApplicationResponseData") {

        describe("parse") {
            context("Happy case") {
                val result = CreateLoanApplicationResponseData.parse("GECAL|HP|14|610410008|123456|R15|KORAT")
                it ("Return correct company") {
                    assertEquals("GECAL", result.company)
                }
                it ("Return correct productCode") {
                    assertEquals("HP", result.productCode)
                }
                it ("Return correct branchCode") {
                    assertEquals("14", result.branchCode)
                }
                it ("Return correct loanId") {
                    assertEquals("610410008", result.loanId)
                }
                it ("Return correct loanId") {
                    assertEquals("123456", result.jetNumber)
                }
                it ("Return correct loanId") {
                    assertEquals("R15", result.regionCode)
                }
                it ("Return correct loanId") {
                    assertEquals("KORAT", result.hubId)
                }
            }
            context("Unhappy case") {
                val result = CreateLoanApplicationResponseData.parse("|||0|||")
                it ("Return correct company") {
                    assertEquals("", result.company)
                }
                it ("Return correct productCode") {
                    assertEquals("", result.productCode)
                }
                it ("Return correct branchCode") {
                    assertEquals("", result.branchCode)
                }
                it ("Return correct loanId") {
                    assertEquals("0", result.loanId)
                }
                it ("Return correct loanId") {
                    assertEquals("", result.jetNumber)
                }
                it ("Return correct loanId") {
                    assertEquals("", result.regionCode)
                }
                it ("Return correct loanId") {
                    assertEquals("", result.hubId)
                }
            }
        }
    }
})