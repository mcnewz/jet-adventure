package com.jet.service.adventure.ewi.service
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.context
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.ZonedDateTime
import kotlin.test.assertEquals
import kotlin.test.assertFails

class AfsSerializerTests : Spek ({

    describe("AfsSerializer") {
        val serializer = AfsSerializer()

        context("with only primitive fields") {
            data class MyClass(
                @AfsField val myStr: String,
                @AfsField val myInt: Int,
                val someNormalField: String
            )

            it("Return correct AFS String") {
                val myObj = MyClass("foo", 12345, "bar")
                val result = serializer.serialize(myObj)
                assertEquals("foo|12345|", result)
            }
        }

        context("with date and time fields") {
            data class MyClass(
                @AfsField val myLocalDate: LocalDate,
                @AfsField val myLocalTime: LocalTime,
                @AfsField val myLocalDateTime: LocalDateTime,
                @AfsField val myZonedDateTime: ZonedDateTime
            )

            it("Convert fields to String") {
                val myObj = MyClass(
                        LocalDate.parse("2018-05-22"),
                        LocalTime.parse("11:11:11"),
                        LocalDateTime.parse("2018-05-22T11:11:11"),
                        ZonedDateTime.parse("2018-05-22T11:11:11+07:00")
                )
                val result = serializer.serialize(myObj)
                assertEquals("2018-05-22|11:11:11|2018-05-22T11:11:11|2018-05-22T11:11:11+07:00|", result)
            }

            it("Use provided pattern to convert date and time") {
                data class MyClass(@AfsField(pattern = "dd/MM/yyyy hh:mm:ss") val dt: ZonedDateTime)
                val myObj = MyClass(ZonedDateTime.parse("2018-05-22T11:11:11+07:00"))
                val result = serializer.serialize(myObj)
                assertEquals("22/05/2018 11:11:11|", result)
            }

            it("Throw exception if pattern is invalid") {
                data class MyClass(@AfsField(pattern = "dd/MM/yyyy hh:mm:ss") val d: LocalDate)
                val myObj = MyClass(LocalDate.parse("2018-05-22"))
                assertFails {
                    serializer.serialize(myObj)
                }
            }
        }

        context("With nullable fields") {
            data class MyClass(
                @AfsField val foo: String?,
                @AfsField val bar: Int
            )
            it("Serialize null as an empty string") {
                val myObj = MyClass(null, 123)
                val result = serializer.serialize(myObj)
                assertEquals("|123|", result)
            }
        }
    }
})