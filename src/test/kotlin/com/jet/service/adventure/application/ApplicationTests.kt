package com.jet.service.adventure.application

import com.fasterxml.jackson.databind.ObjectMapper
import com.jet.service.adventure.ewi.exception.EwiResponseException
import com.jet.service.adventure.ewi.service.EwiClient
import com.jet.service.adventure.fixture.DocumentRequestFixture
import com.jet.service.adventure.fixture.LoanApplicationRequestFixture
import com.jet.service.adventure.idoc.exception.IDocGenerateDocumentSetIdException
import com.jet.service.adventure.idoc.exception.IDocGetDocumentSetException
import com.jet.service.adventure.idoc.exception.IDocSubmitException
import com.jet.service.adventure.idoc.exception.IDocUpdateApplicationNoException
import com.jet.service.adventure.ifu.exception.IFuGetRepositoryException
import com.jet.service.adventure.ifu.exception.IFuUploadException
import com.jet.service.adventure.service.DocumentService
import com.jet.service.adventure.service.ImageService
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.eq
import com.nhaarman.mockito_kotlin.whenever
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.mock.web.MockMultipartFile
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.context.web.WebAppConfiguration
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.context.WebApplicationContext
import kotlin.test.assertEquals

@ExtendWith(SpringExtension::class)
@ContextConfiguration
@WebAppConfiguration
@SpringBootTest
@ActiveProfiles("EWI_STUB_ACCESS,IFU_STUB_ACCESS,IDOC_STUB_ACCESS,AWS_SECRET_STUB_ACCESS")
class ApplicationTests(
    context: WebApplicationContext
) {

    @MockBean
    private lateinit var mockClient: EwiClient

    @MockBean
    private lateinit var imageService: ImageService

    @MockBean
    private lateinit var documentService: DocumentService

    @Autowired
    private lateinit var jsonMapper: ObjectMapper
    private val mvc: MockMvc = MockMvcBuilders.webAppContextSetup(context).build()
    private val loanApplicationRequest = LoanApplicationRequestFixture.build()

    @Test
    fun createDocumentSetSuccess() {
        whenever(documentService.submit(any())).thenReturn("1807061547457891")
        val json = jsonMapper.writeValueAsString(DocumentRequestFixture.build())
        mvc.perform(
                post("/document")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json)
        ).andExpect(status().isCreated)
    }

    @Test
    fun handlerIDocSubmitException() {
        whenever(documentService.submit(any())).thenThrow(IDocSubmitException("Idoc not submit"))
        val json = jsonMapper.writeValueAsString(DocumentRequestFixture.build())
        mvc.perform(
                post("/document")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json)
        ).andExpect(status().isServiceUnavailable)
    }

    @Test
    fun handlerIDocGenerateDocumentSetIdException() {
        whenever(documentService.submit(any())).thenThrow(IDocGenerateDocumentSetIdException("Idoc not generate"))
        val json = jsonMapper.writeValueAsString(DocumentRequestFixture.build())
        mvc.perform(
                post("/document")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json)
        ).andExpect(status().isServiceUnavailable)
    }

    @Test
    fun handlerIDocGetDocumentSetException() {
        whenever(documentService.submit(any())).thenThrow(IDocGetDocumentSetException("Idoc not get documentSet"))
        val json = jsonMapper.writeValueAsString(DocumentRequestFixture.build())
        mvc.perform(
                post("/document")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json)
        ).andExpect(status().isServiceUnavailable)
    }

    @Test
    fun handlerIDocUpdateApplicationNoException() {
        whenever(documentService.submit(any())).thenThrow(IDocUpdateApplicationNoException("Idoc not update applicationNo"))
        val json = jsonMapper.writeValueAsString(DocumentRequestFixture.build())
        mvc.perform(
                post("/document")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json)
        ).andExpect(status().isServiceUnavailable)
    }

    @Test
    fun documentUploadSuccess() {
        whenever(imageService.upload(any(), any())).thenReturn("some-file-path")
        val file = MockMultipartFile("file", "JETID1", "image/jpeg", "FOO".toByteArray())
        mvc.perform(
                multipart("/document/file")
                        .file(file)
                        .param("productType", "FOUR_WHEELER")
        ).andExpect(status().isCreated)
    }

    @Test
    fun handlerIFuUploadException() {
        whenever(imageService.upload(any(), any())).thenThrow(IFuUploadException("Cannot upload to iFu"))
        val file = MockMultipartFile("file", "JETID1", "image/jpeg", "FOO".toByteArray())
        mvc.perform(
                multipart("/document/file")
                        .file(file)
                        .param("productType", "FOUR_WHEELER")
        ).andExpect(status().isServiceUnavailable)
    }

    @Test
    fun handlerIFuGetRepositoryException() {
        whenever(imageService.upload(any(), any())).thenThrow(IFuGetRepositoryException("Repository not found"))
        val file = MockMultipartFile("file", "JETID1", "image/jpeg", "FOO".toByteArray())
        mvc.perform(
                multipart("/document/file")
                        .file(file)
                        .param("productType", "FOUR_WHEELER")
        ).andExpect(status().isInternalServerError)
    }

    @Test
    fun handleEwiResponseException() {
        whenever(mockClient.send(any(), eq("jet-id"), any(), any())).thenThrow(EwiResponseException("message", "code"))
        val json = jsonMapper.writeValueAsString(loanApplicationRequest)
        mvc.perform(
                post("/loan-application")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json)
        ).andExpect(status().isServiceUnavailable)
    }

    @Test
    fun handleAfsResponseException() {
        val failPDataOut = "นาย|th fn ทดสอบ|th ln ทดสอบ||en fn test|en ln test|05102529||1100300324569|05102566||||||10110|||45980.00|New Car||0840941611|13042561|090015|||||1505|Y|S1:Y,S2:N,S3:Y,S4:Y,S5:Y,S6:Y|N||No--t|                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      0016-Invalid Maketing Date.                                                                                                                                                                             GECAL|HP|14|610410008|123456|R15|KORAT"
        whenever(mockClient.send(any(), eq("jet-id"), any(), any())).thenReturn(failPDataOut)
        val json = jsonMapper.writeValueAsString(loanApplicationRequest)
        mvc.perform(
                post("/loan-application")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json)
        ).andExpect(status().isBadRequest)
    }

    @Test
    fun handleEmptyJetId() {
        val request = loanApplicationRequest.copy(jetId = "")
        val json = jsonMapper.writeValueAsString(request)
        mvc.perform(
                post("/loan-application")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json)
        ).andExpect(status().isBadRequest)
    }

    @Test
    fun createLoanApplicationSuccess() {
        val successPDataOut = "นาย|th fn ทดสอบ|th ln ทดสอบ||en fn test|en ln test|05102529||1100300324569|05102566||||||10110|||45980.00|New Car||0840941611|13042561|090015|||||1505|Y|S1:Y,S2:N,S3:Y,S4:Y,S5:Y,S6:Y|N||No--t|                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      0000-Success                                                                                                                                                                                            GECAL|HP|14|610410008|123456|R15|KORAT"
        whenever(mockClient.send(any(), eq("jet-id"), any(), any())).thenReturn(successPDataOut)
        val json = jsonMapper.writeValueAsString(loanApplicationRequest)
        val result = mvc.perform(
                post("/loan-application")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json)
        ).andExpect(status().isCreated).andReturn()

        assertEquals(
                "{\"loanApplicationID\":\"610410008\",\"company\":\"GECAL\",\"productType\":\"FOUR_WHEELER\",\"branchCode\":\"14\"," +
                        "\"jetNumber\":\"123456\",\"regionCode\":\"R15\",\"hubId\":\"KORAT\"}",
                result.response.contentAsString
        )
    }
}