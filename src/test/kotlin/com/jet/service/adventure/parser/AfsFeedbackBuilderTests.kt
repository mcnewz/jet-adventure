package com.jet.service.adventure.parser

import com.jet.service.adventure.model.AFSFeedback
import com.jet.service.adventure.model.Application
import com.jet.service.adventure.model.ApplicationStatus
import com.jet.service.adventure.model.MarketRepresentative
import com.jet.service.adventure.model.PreApprove
import com.jet.service.adventure.model.ProductType
import com.jet.service.adventure.model.TriggerCode
import com.jet.service.adventure.model.Verification
import com.jet.service.adventure.model.VerificationStatus
import com.jet.service.adventure.parsers.AfsFeedbackBuilder
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.given
import org.jetbrains.spek.api.dsl.it
import org.junit.jupiter.api.Assertions
import java.time.ZonedDateTime

class AfsFeedbackBuilderTests : Spek({
    given("AfsFeedbackBuilder") {
        it("parse afs data") {
            val feedback = listOf("GECAL", "HP", "22", "610610785", "07", "MK06", "นางสาวสิริกุล นวลจำรัส", "0985814748", "A", "P", "83", "N", "Y", "Y", "06062561", "164559", "", "", "", "")

            val afsFeedback = AFSFeedback(triggerCode = TriggerCode.MANUAL_ASSIGN_MR,
                    application = Application(
                            loanId = "610610785",
                            company = "GECAL",
                            productType = ProductType.FOUR_WHEELER,
                            branch = "22",
                            status = ApplicationStatus.ACTIVE,
                            mr = MarketRepresentative(mobileNumber = "0985814748", code = "MK06", name = "นางสาวสิริกุล นวลจำรัส"),
                            verification = Verification(status = VerificationStatus.PENDING, reasonCode = "83",
                                    citizenIdStatus = false, paySlipStatus = true, statementOrEmployeeCardStatus = true),
                            statusDateTime = ZonedDateTime.parse("2018-06-06T16:45:59+07:00[Asia/Bangkok]"),
                            preApprove = PreApprove(amount = null, term = null, percentDown = null, installment = null)))

            Assertions.assertEquals(afsFeedback, AfsFeedbackBuilder.parseAndBuild(feedback))
        }
    }
})