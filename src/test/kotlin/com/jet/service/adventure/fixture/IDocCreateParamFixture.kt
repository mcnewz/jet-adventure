package com.jet.service.adventure.fixture

import com.jet.service.adventure.idoc.model.IDocCreateParam
import java.time.LocalDate

class IDocCreateParamFixture {
    companion object {
        fun build(): IDocCreateParam {
            return IDocCreateParam(
                "firstName",
                "lastName",
                LocalDate.parse("1986-01-01"),
                "1000000000009",
                "6105234278",
                "18",
                "BKK",
                "HP",
                listOf("some-file-path")
            )
        }
    }
}