package com.jet.service.adventure.fixture

import com.jet.service.adventure.model.DocumentRequest
import com.jet.service.adventure.model.ProductType
import java.time.LocalDate

class DocumentRequestFixture {
    companion object {
        fun build(): DocumentRequest {
            return DocumentRequest(
                null,
                "6105234278",
                "firstName",
                "lastName",
                LocalDate.parse("1986-01-01"),
                "1000000000009",
                "18",
                "BKK",
                ProductType.FOUR_WHEELER,
                listOf("some-file-path")
            )
        }
    }
}