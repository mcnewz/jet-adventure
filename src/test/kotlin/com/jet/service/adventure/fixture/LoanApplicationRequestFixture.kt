package com.jet.service.adventure.fixture

import com.jet.service.adventure.model.LoanApplicationRequest
import com.jet.service.adventure.model.LoanUsage
import com.jet.service.adventure.model.Product
import java.time.LocalDate
import java.time.ZonedDateTime

class LoanApplicationRequestFixture {
    companion object {
        fun build(): LoanApplicationRequest {
            return LoanApplicationRequest(
                jetId = "jet-id",
                thTitle = "นาย",
                thFirstName = "จิตติ",
                thSurname = "เจ๊ด",
                enTitle = "mr",
                enFirstName = "Jitti",
                enSurname = "Jet",
                birthDate = LocalDate.parse("1985-04-28"),
                idCard = "1000000000009",
                idCardExpire = LocalDate.parse("2020-04-28"),
                zipCode = "10600",
                income = "100000",
                product = Product.NEW_CAR,
                mobileNo = "0870765114",
                marketingDateTime = ZonedDateTime.parse("2018-04-12T00:00:00+07:00"),
                yearOfService = 2,
                applyForLoan = false,
                loanUsage = LoanUsage(
                    vehicle = false,
                    resident = false,
                    creditCard = false,
                    overDraft = false,
                    personalLoan = false
                ),
                overdue = false
            )
        }
    }
}