package com.jet.service.adventure.fixture

import com.jet.service.adventure.ewi.model.CreateLoanApplicationParam

class CreateLoanApplicationParamFixture {
    companion object {
        fun build(): CreateLoanApplicationParam {
            return CreateLoanApplicationParam(
                "นาย",
                "จิตติ",
                "เจ๊ด",
                "mr",
                "Jitti",
                "Jet",
                "28042528",
                "",
                "1000000000009",
                "28042563",
                "",
                "",
                "",
                "",
                "",
                "10600",
                "",
                "",
                "100000",
                "New Car",
                "",
                "0870765114",
                "12042561",
                "000000",
                "",
                "",
                "",
                "",
                "0002",
                "N",
                "VEHICLE:N,RESIDENT:N,CREDIT_CARD:N,OVER_DRAFT:N,PERSONAL_LOAN:N",
                "N",
                "jet-id",
                ""
            )
        }
    }
}