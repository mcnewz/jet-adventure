package com.jet.service.adventure.fixture

import com.jet.service.adventure.idoc.model.IDocAppendParam

class IDocAppendParamFixture {
    companion object {
        fun build(): IDocAppendParam {
            return IDocAppendParam(
                    "1807091425365432",
                    "HP",
                    "BKK",
                    "18",
                    listOf("some-file-path")
            )
        }
    }
}