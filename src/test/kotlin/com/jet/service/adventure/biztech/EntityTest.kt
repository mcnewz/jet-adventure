package com.jet.service.adventure.biztech

import com.jet.service.adventure.biztech.exception.InvalidEntityException
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.context
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class EntityTest : Spek({

    describe("Entity") {

        describe("fromProductCode") {
            context("Four wheel products") {
                val productCodes = listOf("HP")
                productCodes.forEach {
                    val result = Entity.fromProductCode(it)
                    it ("return BAY for $it") {
                        assertEquals(Entity.BAY, result)
                    }
                }
            }

            context("Two wheel products") {
                val productCodes = listOf("MC", "BB")
                productCodes.forEach {
                    val result = Entity.fromProductCode(it)
                    it("return AYCAL for $it") {
                        assertEquals(Entity.AYCAL, result)
                    }
                }
            }

            context("Invalid Product Code") {

                it("Throw InvalidEntityException") {
                    assertFailsWith(InvalidEntityException::class) {
                        Entity.fromProductCode("LS")
                    }
                }
            }
        }
    }
})