package com.jet.service.adventure.biztech

import com.biztech.ifu.service.FileUploadService
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import kotlin.test.assertTrue

class WebServiceTests : Spek({

    describe("WebService") {

        describe("build") {

            val fileUploadService = WebService.build<FileUploadService>(
                    "some-endpoint", 50000)

            it ("return object of correct class") {
                assertTrue(fileUploadService is FileUploadService)
            }
        }
    }
})