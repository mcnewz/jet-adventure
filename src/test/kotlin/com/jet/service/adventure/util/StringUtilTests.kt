package com.jet.service.adventure.util

import com.jet.service.adventure.exceptions.AfsFeedbackParsingException
import com.jet.service.adventure.model.ApplicationStatus
import com.jet.service.adventure.model.ProductType
import com.jet.service.adventure.model.TriggerCode
import com.jet.service.adventure.model.VerificationStatus
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.context
import org.jetbrains.spek.api.dsl.given
import org.jetbrains.spek.api.dsl.it
import java.time.ZoneId
import java.time.ZonedDateTime
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class StringUtilTests : Spek({

    given("a StringUtils") {
        context("TriggerCode Conversions") {
            it("converts string to relevant Trigger Code") {
                assertEquals("01".toTriggerCode(), TriggerCode.UPDATE_INFORMATION)
                assertEquals("02".toTriggerCode(), TriggerCode.REJECT)
                assertEquals("03".toTriggerCode(), TriggerCode.PRE_APPROVED)
                assertEquals("04".toTriggerCode(), TriggerCode.HARD_CASE)
                assertEquals("05".toTriggerCode(), TriggerCode.CREDIT_LINE_EXPIRED)
                assertEquals("06".toTriggerCode(), TriggerCode.LOAN_UTILIZED)
                assertEquals("07".toTriggerCode(), TriggerCode.MANUAL_ASSIGN_MR)
                assertEquals("08".toTriggerCode(), TriggerCode.RETAKE_PHOTO)
                assertEquals("09".toTriggerCode(), TriggerCode.CANCEL)
                assertEquals("10".toTriggerCode(), TriggerCode.OTHERS)
            }

            it("throws exception when converting invalid string to Trigger Code") {
                assertFailsWith(AfsFeedbackParsingException::class) {
                    assertEquals("".toTriggerCode(), TriggerCode.CANCEL)
                }
            }
        }

        context("Boolean Conversions") {
            it("returns true/false when given valid input") {
                assertEquals("Y".toBoolean(), true)
                assertEquals("N".toBoolean(), false)
                assertEquals("TRUE".toBoolean(), true)
                assertEquals("FALSE".toBoolean(), false)
            }

            it("returns false when given invalid input") {
                assertEquals("B".toBoolean(), false)
                assertEquals("".toBoolean(), false)
            }
        }

        context("ProductType Conversions") {
            it("returns ProductType when given valid input") {
                assertEquals("HP".toProductType(), ProductType.FOUR_WHEELER)
                assertEquals("MC".toProductType(), ProductType.TWO_WHEELER)
            }

            it("throws exception when converting invalid string to ProductType") {
                assertFailsWith(AfsFeedbackParsingException::class) {
                    assertEquals("ABC".toProductType(), ProductType.FOUR_WHEELER)
                }
            }
        }

        context("ApplicationStatus Conversions") {
            it("returns ApplicationStatus when given valid input") {
                assertEquals("A".toApplicationStatus(), ApplicationStatus.ACTIVE)
                assertEquals("C".toApplicationStatus(), ApplicationStatus.CONTRACTED)
                assertEquals("D".toApplicationStatus(), ApplicationStatus.AUTO_CANCEL)
                assertEquals("".toApplicationStatus(), ApplicationStatus.NOT_ACTIVE)
            }

            it("throws exception when converting invalid string to ApplicationStatus") {
                assertFailsWith(AfsFeedbackParsingException::class) {
                    assertEquals("N".toApplicationStatus(), ApplicationStatus.ACTIVE)
                }
            }
        }

        context("VerificationStatus Conversions") {
            it("returns VerificationStatus when given valid input") {
                assertEquals("R".toApplicationVerificationStatus(TriggerCode.CANCEL, ""), VerificationStatus.REJECT)
                assertEquals("D".toApplicationVerificationStatus(TriggerCode.CANCEL, ""), VerificationStatus.MANUAL_CANCEL)
                assertEquals("Y".toApplicationVerificationStatus(TriggerCode.CANCEL, ""), VerificationStatus.UTILIZE)
            }

            context("when Application VerifyStatus is 'P'") {
                it("returns PRE_APPROVED when trigger code is PRE_APPROVED") {
                    assertEquals("P".toApplicationVerificationStatus(TriggerCode.PRE_APPROVED, "A"), VerificationStatus.PRE_APPROVED)
                }
                it("returns PRE_APPROVED when trigger code is not HARD_CASE/CANCEL and reasonCode is 81") {
                    assertEquals("P".toApplicationVerificationStatus(TriggerCode.PRE_APPROVED, "81"), VerificationStatus.PRE_APPROVED)
                    assertEquals("P".toApplicationVerificationStatus(TriggerCode.RETAKE_PHOTO, "81"), VerificationStatus.PRE_APPROVED)
                    assertEquals("P".toApplicationVerificationStatus(TriggerCode.UPDATE_INFORMATION, "81"), VerificationStatus.PRE_APPROVED)
                }
                it("returns PENDING when trigger code is not HARD_CASE/CANCEL and reasonCode is not 81") {
                    assertEquals("P".toApplicationVerificationStatus(TriggerCode.RETAKE_PHOTO, ""), VerificationStatus.PENDING)
                    assertEquals("P".toApplicationVerificationStatus(TriggerCode.UPDATE_INFORMATION, ""), VerificationStatus.PENDING)
                }
                it("returns HARD_CASE when trigger code is HARD_CASE") {
                    assertEquals("P".toApplicationVerificationStatus(TriggerCode.HARD_CASE, "A"), VerificationStatus.HARD_CASE)
                }
                it("returns AUTOMATIC_CANCEL when trigger code is CANCEL") {
                    assertEquals("P".toApplicationVerificationStatus(TriggerCode.CANCEL, "A"), VerificationStatus.AUTOMATIC_CANCEL)
                }
            }

            it("throws exception when converting invalid string to VerificationStatus") {
                assertFailsWith(AfsFeedbackParsingException::class) {
                    assertEquals("N".toApplicationVerificationStatus(TriggerCode.CANCEL, ""), VerificationStatus.PENDING)
                }
            }
        }

        context("ZonedDateTime Conversions") {
            it("returns ZonedDateTime when given date and time values") {
                val zoneId = ZoneId.of("Asia/Bangkok")
                val expectedDateTime = ZonedDateTime.of(2018, 7, 5, 23, 9, 30, 0, zoneId)
                assertEquals("05072561".toZonedDateTime("230930"), expectedDateTime)
            }
        }
    }
})