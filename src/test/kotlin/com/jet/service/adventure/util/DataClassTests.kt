package com.jet.service.adventure.util

import org.assertj.core.api.Assertions.assertThat
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.context
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import kotlin.test.assertEquals

class DataClassTests : Spek({

    describe("mask") {
        context("revealFront plus revealBack less than length") {
            it ("mask characters in the middle") {
                val result = mask("0812345678", '*', 4, 5)
                assertEquals("0812*45678", result)
            }
        }
        context("revealFront is 0") {
            it("mask from start of the string") {
                val result = mask("0812345678", '*', 0, 5)
                assertEquals("*****45678", result)
            }
        }
        context("revealBack is 0") {
            it("mask from end of the string") {
                val result = mask("0812345678", '*', 7, 0)
                assertEquals("0812345***", result)
            }
        }
        context("revealFront plus revealBack more than length") {
            it ("not mask characters") {
                val result = mask("0812345678", '*', 7, 5)
                assertEquals("0812345678", result)
            }
        }
        context("revealFront plus revealBack equal to length") {
            it ("not mask characters") {
                val result = mask("0812345678", '*', 7, 3)
                assertEquals("0812345678", result)
            }
        }
    }

    describe("dataClassToString") {

        context("default annotation parameter") {
            data class Customer(val name: String = "foo", @MaskedField val phoneNumber: String = "0812345678")
            it ("convert data class to string and mask field with X") {
                val result = dataClassToString(Customer())
                assertThat(result).contains("Customer(name=foo, phoneNumber=08XXXXXX78")
            }
        }
        context("override mask char") {
            data class Customer(val name: String = "foo", @MaskedField('#') val phoneNumber: String = "0812345678")
            it ("convert data class to string and mask field with #") {
                val result = dataClassToString(Customer())
                assertThat(result).contains("Customer(name=foo, phoneNumber=08######78")
            }
        }
        context("override reveal") {
            data class Customer(val name: String = "foo", @MaskedField(revealFront = 4, revealBack = 1) val phoneNumber: String = "0812345678")
            it ("convert data class to string and mask field with X and reveal correct amount of characters") {
                val result = dataClassToString(Customer())
                assertThat(result).contains("Customer(name=foo, phoneNumber=0812XXXXX8")
            }
        }
    }
})