package com.jet.service.adventure.service

import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.junit.jupiter.api.Assertions
import org.springframework.mock.web.MockMultipartFile

class ImageEncoderServiceTests : Spek ({

    describe("ImageEncoderService") {

        describe("encodeToBase64") {

            val imageId = MockMultipartFile("JETID", "JETID1", "image/jpeg", "FOO".toByteArray())
            val stringBase64 = ImageEncoderService.encodeToBase64(imageId)

            it("encoder file to base64 string") {
                Assertions.assertEquals("Rk9P", stringBase64)
            }
        }
    }
})