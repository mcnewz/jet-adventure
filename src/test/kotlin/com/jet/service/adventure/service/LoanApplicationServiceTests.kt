package com.jet.service.adventure.service

import com.jet.service.adventure.ewi.model.CreateLoanApplicationResult
import com.jet.service.adventure.ewi.model.MarkFlagParam
import com.jet.service.adventure.ewi.service.EwiService
import com.jet.service.adventure.fixture.CreateLoanApplicationParamFixture
import com.jet.service.adventure.fixture.LoanApplicationRequestFixture
import com.jet.service.adventure.model.LoanApplicationDocumentAppendRequest
import com.jet.service.adventure.model.LoanApplicationDocumentAppendResponse
import com.jet.service.adventure.model.LoanApplicationResponse
import com.jet.service.adventure.model.ProductType
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.eq
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.reset
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import kotlin.test.assertEquals

class LoanApplicationServiceTests : Spek ({

    describe("LoanApplicationService") {
        val ewiService = mock<EwiService>()
        val loanApplicationService = LoanApplicationService(ewiService)

        describe("create") {
            val request = LoanApplicationRequestFixture.build()
            val param = CreateLoanApplicationParamFixture.build()
            val result = CreateLoanApplicationResult(
                company = "GECAL",
                productCode = "HP",
                branchCode = "18",
                loanId = "610765883",
                jetNumber = "jet-id",
                regionCode = "BKK",
                hubId = "BKK"
            )
            var response: LoanApplicationResponse? = null
            beforeEachTest {
                reset(ewiService)
                whenever(ewiService.createLoanApplication(any(), eq(ProductType.FOUR_WHEELER))).thenReturn(result)
                response = loanApplicationService.create(request)
            }

            it("return correct response") {
                val expected = LoanApplicationResponse(
                    loanApplicationID = "610765883",
                    company = "GECAL",
                    productType = ProductType.FOUR_WHEELER,
                    hubId = "BKK",
                    branchCode = "18",
                    regionCode = "BKK",
                    jetNumber = "jet-id"
                )
                assertEquals(expected, response)
            }

            it("send correct param to createLoanApplication") {
                verify(ewiService).createLoanApplication(eq(param), eq(ProductType.FOUR_WHEELER))
            }
        }

        describe("setAppend") {
            val request = LoanApplicationDocumentAppendRequest(
                    ProductType.FOUR_WHEELER,
                    "48",
                    "610765883",
                    "79"
            )

            val markFlagParam = MarkFlagParam(
                    "GECAL",
                    "HP",
                    "48",
                    "610765883",
                    "A", "JT", "79"
            )

            var response: LoanApplicationDocumentAppendResponse? = null
            beforeEachTest {
                reset(ewiService)
                response = loanApplicationService.setAppend(request)
            }

            it("send correct param to markFlag") {
                verify(ewiService).markFlag(eq(markFlagParam), eq(ProductType.FOUR_WHEELER))
            }

            it("return correct response") {
                val expected = LoanApplicationDocumentAppendResponse("79")
                assertEquals(expected, response)
            }
        }
    }
})