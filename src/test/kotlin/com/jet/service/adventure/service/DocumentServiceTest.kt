package com.jet.service.adventure.service

import com.biztech.idociapps.vo.CreditDocumentSetVO
import com.jet.service.adventure.fixture.DocumentRequestFixture
import com.jet.service.adventure.fixture.IDocAppendParamFixture
import com.jet.service.adventure.fixture.IDocCreateParamFixture
import com.jet.service.adventure.idoc.IDocService
import com.jet.service.adventure.idoc.model.IDocCustomerInfo
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.eq
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.reset
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.context
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import java.time.LocalDate
import kotlin.test.assertEquals

class DocumentServiceTest : Spek({

    describe("DocumentService") {
        val iDocService = mock<IDocService>()
        val documentService = DocumentService(iDocService)

        describe("submit") {
            beforeEachTest {
                reset(iDocService)
            }

            val request = DocumentRequestFixture.build().copy(documentSetId = "180704172654787")

            context("document set exists by application no") {
                val params = IDocAppendParamFixture.build().copy(documentSetId = "180704172654788")
                beforeEachTest {
                    whenever(iDocService.getDocumentSetIdByApplicationNo(eq("HP"), eq("186105234278"))).thenReturn("180704172654788")
                    whenever(iDocService.append(eq(params))).thenReturn("180704172654788")
                }

                it("return correct documentSetId from iDocService.append") {
                    val result = documentService.submit(request)
                    assertEquals("180704172654788", result)
                }
            }

            context("document set does not exists by application no") {
                beforeEachTest {
                    whenever(iDocService.getDocumentSetIdByApplicationNo(any(), any())).thenReturn(null)
                }

                context("documentSetId is not null") {
                    val request = DocumentRequestFixture.build().copy(documentSetId = "180704172654787")

                    context("document set exists by document set id") {
                        val params = IDocAppendParamFixture.build().copy(documentSetId = "180704172654787")
                        lateinit var creditDocumentSet: CreditDocumentSetVO
                        lateinit var result: String
                        beforeEachTest {
                            creditDocumentSet = CreditDocumentSetVO()
                            creditDocumentSet.id = "180704172654787"
                            whenever(iDocService.getDocumentSetById(eq("HP"), eq("180704172654787"))).thenReturn(creditDocumentSet)
                            whenever(iDocService.append(eq(params))).thenReturn("180704172654787")
                            result = documentService.submit(request)
                        }
                        it("return correct documentSetId from iDocService.append") {
                            assertEquals("180704172654787", result)
                        }
                        it("un reset data") {
                            verify(iDocService).unResetData(
                                eq("HP"), eq(creditDocumentSet), eq("186105234278"), eq(IDocCustomerInfo(
                                    "firstName",
                                    "lastName",
                                    LocalDate.parse("1986-01-01"),
                                    "1000000000009"
                            )))
                        }
                    }

                    context("document set does not exists") {
                        val params = IDocCreateParamFixture.build()
                        beforeEachTest {
                            whenever(iDocService.getDocumentSetById(any(), any())).thenReturn(null)
                            whenever(iDocService.create(eq(params))).thenReturn("180704172654789")
                        }
                        it("return correct documentSetId from iDocService.create") {
                            val result = documentService.submit(request)
                            assertEquals("180704172654789", result)
                        }
                    }
                }

                context("documentSetId is null") {
                    val request = DocumentRequestFixture.build()
                    val params = IDocCreateParamFixture.build()
                    beforeEachTest {
                        whenever(iDocService.create(eq(params))).thenReturn("180704172654789")
                    }
                    it("return correct documentSetId from iDocService.create") {
                        val result = documentService.submit(request)
                        assertEquals("180704172654789", result)
                    }
                }
            }
        }
    }
})