package com.jet.service.adventure.service

import com.biztech.idociapps.vo.CreditDocumentSetVO
import com.jet.service.adventure.biztech.Entity
import com.jet.service.adventure.fixture.IDocAppendParamFixture
import com.jet.service.adventure.fixture.IDocCreateParamFixture
import com.jet.service.adventure.idoc.IDocClient
import com.jet.service.adventure.idoc.IDocService
import com.jet.service.adventure.idoc.model.IDocConfigurationProperties
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.check
import com.nhaarman.mockito_kotlin.eq
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.reset
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.context
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import kotlin.test.assertEquals

class IDocServiceTest : Spek({

    describe("IDocService") {

        val iDocClient = mock<IDocClient>()
        val config = IDocConfigurationProperties()
        config.applicantType = "I"
        config.mode = "JPEG"
        config.documentSourceChannel = "JET"
        config.newOriginateMode = "ISMART_NEWDOC"
        config.appendOriginateMode = "IBUDDY_MR_APPEND"
        config.sender = "SYSTEM"
        config.userAgent = "JET_AGENT"
        config.bay.documentMonitorObjIdPrefix = "IAPPS_"
        config.bay.monitorId = "IAPPS_WS01"
        config.bay.documentProcessId = "IAPPS_WS01"

        val iDocService = IDocService(iDocClient, config)

        describe("create") {

            val iDocCreateParam = IDocCreateParamFixture.build()

            var documentSetId = ""

            beforeEachTest {
                reset(iDocClient)
                whenever(iDocClient.generateDocumentSetId(any())).thenReturn("180701164210666")
                documentSetId = iDocService.create(iDocCreateParam)
            }

            it("return correct documentSetId") {
                assertEquals("180701164210666", documentSetId)
            }

            it ("send correct entity to generateDocumentSetId") {
                verify(iDocClient).generateDocumentSetId(eq(Entity.BAY))
            }

            it("send correct param to execDocumentProcess") {
                verify(iDocClient).execDocumentProcess(eq(Entity.BAY), check {
                    assertEquals("some-file-path", it.documentProcessDetailVOs[0].sourcePath)
                    assertEquals("PHOTO_JOTHER", it.documentProcessDetailVOs[0].documentType)
                    assertEquals("186105234278", it.applicationNo)
                    assertEquals("firstName", it.customerFirstName)
                    assertEquals("lastName", it.customerLastName)
                    assertEquals("1000000000009", it.citizenId)
                    assertEquals("18", it.branchId)
                    assertEquals("BKK", it.hubId)
                    assertEquals("HP", it.productCode)
                    assertEquals("JPEG", it.mode)
                    assertEquals("JET", it.documentSourceChannel)
                    assertEquals("SYSTEM", it.sender)
                    assertEquals("JET_AGENT", it.userAgent)
                    assertEquals("ISMART_NEWDOC", it.originateMode)
                    assertEquals("IAPPS_BKK", it.documentMonitorObjId)
                    assertEquals("IAPPS_WS01", it.documentProcessId)
                    assertEquals("IAPPS_WS01", it.monitorId)
                    assertEquals("I", it.applicantType)
                })
            }
        }

        describe("append") {

            val iDocAppendParam = IDocAppendParamFixture.build()

            var documentSetId = ""
            beforeEachTest {
                reset(iDocClient)
                documentSetId = iDocService.append(iDocAppendParam)
            }

            it("return correct documentSetId") {
                assertEquals("1807091425365432", documentSetId)
            }

            it("send correct param to execDocumentProcess") {
                verify(iDocClient).execDocumentProcess(eq(Entity.BAY), check {
                    assertEquals("some-file-path", it.documentProcessDetailVOs[0].sourcePath)
                    assertEquals("PHOTO_JOTHER", it.documentProcessDetailVOs[0].documentType)
                    assertEquals("JPEG", it.mode)
                    assertEquals("SYSTEM", it.sender)
                    assertEquals("1807091425365432", it.appendDocumentSetId)
                    assertEquals("HP", it.appendProductCode)
                    assertEquals("IBUDDY_MR_APPEND", it.originateMode)
                    assertEquals("IAPPS_BKK", it.documentMonitorObjId)
                    assertEquals("IAPPS_WS01", it.documentProcessId)
                    assertEquals("IAPPS_WS01", it.monitorId)
                    assertEquals("BKK", it.hubId)
                    assertEquals("18", it.branchId)
                })
            }
        }

        describe("getDocumentSetIdByApplicationNo") {

            val creditDocumentSetVO = CreditDocumentSetVO()
            creditDocumentSetVO.id = "1807091425365432"

            var documentSetId: String? = null

            context("Document set is exists") {
                beforeEachTest {
                    reset(iDocClient)
                    whenever(iDocClient.getDocumentSetByApplicationNo(any(), any())).thenReturn(creditDocumentSetVO)
                    documentSetId = iDocService.getDocumentSetIdByApplicationNo("HP", "1809876566783")
                }

                it("return correct documentSetId") {
                    assertEquals("1807091425365432", documentSetId)
                }

                it ("send correct param to getDocumentSetByApplicationNo") {
                    verify(iDocClient).getDocumentSetByApplicationNo(eq(Entity.BAY), eq("1809876566783"))
                }
            }

            context("Document set is not exists") {
                beforeEachTest {
                    reset(iDocClient)
                    whenever(iDocClient.getDocumentSetByApplicationNo(any(), any())).thenReturn(null)
                    documentSetId = iDocService.getDocumentSetIdByApplicationNo("HP", "1809876566783")
                }

                it("return null") {
                    assertEquals(null, documentSetId)
                }
            }
        }

        describe("isDocumentSetIdExists") {

            val creditDocumentSetVO = CreditDocumentSetVO()
            creditDocumentSetVO.id = "1807091425365432"
            beforeEachTest {
                reset(iDocClient)
                whenever(iDocClient.getDocumentSetById(eq(Entity.BAY), eq("1807091425365432"))).thenReturn(creditDocumentSetVO)
                whenever(iDocClient.getDocumentSetById(eq(Entity.BAY), eq("1807091425365433"))).thenReturn(null)
            }

            context("Document set is exists") {

                it("return correct result") {
                    val documentSet = iDocService.getDocumentSetById("HP", "1807091425365432")
                    assertEquals("1807091425365432", creditDocumentSetVO.id)
                }
            }

            context("Document set is not exists") {

                var documentSet: CreditDocumentSetVO? = null

                beforeEachTest {
                    documentSet = iDocService.getDocumentSetById("HP", "1807091425365433")
                }

                it("return null") {
                    assertEquals(null, documentSet)
                }

                it("send correct parameter to getDocumentSetById") {
                    verify(iDocClient).getDocumentSetById(eq(Entity.BAY), eq("1807091425365433"))
                }
            }
        }
    }
})