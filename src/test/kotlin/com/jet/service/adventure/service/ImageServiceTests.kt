package com.jet.service.adventure.service

import com.jet.service.adventure.biztech.Entity
import com.jet.service.adventure.ifu.IFuClient
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.eq
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.context
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.mockito.Mockito
import org.springframework.mock.web.MockMultipartFile
import kotlin.test.assertEquals

class ImageServiceTests : Spek({

    describe("ImageService") {
        val mockClient = Mockito.mock(IFuClient::class.java)
        val imageService = ImageService(mockClient)

        describe("upload") {
            context("success") {
                val file = MockMultipartFile("JETID", "JETID1", "image/jpeg", "FOO".toByteArray())

                context("BAY") {
                    whenever(mockClient.getRepositoryPath(eq(Entity.BAY))).thenReturn("D:\\BAY")
                    val imageFilePath = imageService.upload(file, Entity.BAY)

                    it ("return file path") {
                        assertEquals(imageFilePath.substring(0, 6), "D:\\BAY")
                    }

                    it ("call correct entity upload method") {
                        verify(mockClient).upload(eq(Entity.BAY), any(), eq("Rk9P"))
                    }
                }

                context("AYCAL") {
                    whenever(mockClient.getRepositoryPath(eq(Entity.AYCAL))).thenReturn("D:\\AYCAL")
                    val imageFilePath = imageService.upload(file, Entity.AYCAL)

                    it ("return file path") {
                        assertEquals(imageFilePath.substring(0, 8), "D:\\AYCAL")
                    }

                    it ("call correct entity upload method") {
                        verify(mockClient).upload(eq(Entity.AYCAL), any(), eq("Rk9P"))
                    }
                }
            }
        }
    }
})