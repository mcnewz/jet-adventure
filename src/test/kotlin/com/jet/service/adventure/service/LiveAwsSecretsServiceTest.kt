package com.jet.service.adventure.service

import com.amazonaws.services.secretsmanager.AWSSecretsManager
import com.amazonaws.services.secretsmanager.model.GetSecretValueRequest
import com.amazonaws.services.secretsmanager.model.GetSecretValueResult
import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import org.assertj.core.api.Assertions
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.context
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.mockito.Mockito
import java.time.LocalDateTime

class LiveAwsSecretsServiceTest : Spek({
    describe("getSecretString") {
        val mockClient = mock<AWSSecretsManager>()
        val mockResult = mock<GetSecretValueResult>()
        val secretService = LiveAwsSecretsService(mockClient, "secret-id")

        val request = GetSecretValueRequest()
                .withSecretId("secret-id")

        whenever(mockResult.secretString).thenReturn("secret-string")
        whenever(mockClient.getSecretValue(request)).thenReturn(mockResult)

        val result = secretService.getSecretString("secret-id")

        it("send correct parameter to aws client") {
            Mockito.verify(mockClient).getSecretValue(request)
        }

        it("return secret string from aws") {
            Assertions.assertThat(result).isEqualTo("secret-string")
        }
    }

    describe("fetchWhenTimeout") {
        val mockClient = mock<AWSSecretsManager>()
        val secretService = LiveAwsSecretsService(mockClient, "")

        context("when secret does not exist") {
            val fetch = mock<() -> String> {
                on { invoke() } doReturn "new-secret"
            }

            val holder = CachedSecretHolder<String>(name = "secret name")

            it("fetches new secret") {
                Assertions.assertThat(secretService.fetchWhenTimeout(holder, fetch)).isEqualTo("new-secret")
            }
        }

        context("when current time is within cache timeout") {
            val fetch = mock<() -> String> {
                on { invoke() } doReturn "new-secret"
            }

            val cacheTimeout: Long = 5

            val holder = CachedSecretHolder(
                    "secret name",
                    CachedSecret(
                            "old-secret",
                            LocalDateTime.now().minusMinutes(cacheTimeout - 1)
                    )
            )

            it("uses old secret") {
                Assertions.assertThat(secretService.fetchWhenTimeout(holder, fetch)).isEqualTo("old-secret")
            }
        }

        context("when current time is after cache timeout") {
            val fetch = mock<() -> String> {
                on { invoke() } doReturn "new-secret"
            }

            val cacheTimeout: Long = 5

            val holder = CachedSecretHolder(
                    "secret name",
                    CachedSecret(
                            "old-secret",
                            LocalDateTime.now().minusMinutes(cacheTimeout)
                    )
            )
            it("fetches new secret") {
                Assertions.assertThat(secretService.fetchWhenTimeout(holder, fetch)).isEqualTo("new-secret")
            }
        }
    }
})