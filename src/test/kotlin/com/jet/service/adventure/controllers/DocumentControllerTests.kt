package com.jet.service.adventure.controllers

import com.jet.service.adventure.biztech.Entity
import com.jet.service.adventure.controller.DocumentController
import com.jet.service.adventure.controller.DocumentResponse
import com.jet.service.adventure.fixture.DocumentRequestFixture
import com.jet.service.adventure.model.ProductType
import com.jet.service.adventure.service.DocumentService
import com.jet.service.adventure.service.ImageService
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.eq
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.context
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.springframework.http.HttpStatus
import org.springframework.mock.web.MockMultipartFile
import kotlin.test.assertEquals

class DocumentControllerTests : Spek({

    describe("DocumentController") {

        val imageService = mock<ImageService>()
        val documentService = mock<DocumentService>()
        val documentController = DocumentController(imageService, documentService)

        describe("upload") {

            val file = MockMultipartFile("JETID", "JETID1", "image/jpeg", "FOO".toByteArray())
            whenever(imageService.upload(any(), any())).thenReturn("D:\\jet\\jet.jpg")
            val response = documentController.upload(ProductType.FOUR_WHEELER, file)

            it("return correct response code") {
                assertEquals(HttpStatus.CREATED, response.statusCode)
            }

            it("return correct file path") {
                assertEquals("D:\\jet\\jet.jpg", response.body?.filePath)
            }

            it("pass correct parameter to imageService.upload") {
                verify(imageService).upload(eq(file), eq(Entity.BAY))
            }
        }

        describe("submit") {

            whenever(documentService.submit(any())).thenReturn("180704172654787")

            context("documentSetId is null") {
                val documentRequest = DocumentRequestFixture.build()
                val response = documentController.submit(documentRequest)
                it("return correct response code") {
                    assertEquals(HttpStatus.CREATED, response.statusCode)
                }

                it("return correct response data") {
                    assertEquals(DocumentResponse(documentRequest.applicationNo, "180704172654787"), response.body)
                }

                it("pass correct parameter to documentService") {
                    verify(documentService).submit(eq(documentRequest))
                }
            }
        }
    }
})