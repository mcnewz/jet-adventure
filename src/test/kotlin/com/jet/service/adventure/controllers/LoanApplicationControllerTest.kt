package com.jet.service.adventure.controllers

import com.jet.service.adventure.configuration.JetConfigurationProperties
import com.jet.service.adventure.controller.LoanApplicationController
import com.jet.service.adventure.model.AFSFeedback
import com.jet.service.adventure.parsers.AfsFeedbackBuilder
import com.jet.service.adventure.service.LoanApplicationService
import com.jet.service.adventure.service.StubAwsSecretsService
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.mockito.Mockito
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.web.client.RestTemplate
import javax.servlet.http.HttpServletRequest

class LoanApplicationControllerTest : Spek({
    val loanApplicationService = mock<LoanApplicationService>()
    val jetConfiguration = JetConfigurationProperties(url = "jet-url")
    val restTemplate = mock<RestTemplate>()
    val awsSecretsService = StubAwsSecretsService()
    val loanApplicationController = LoanApplicationController(loanApplicationService, jetConfiguration, restTemplate, awsSecretsService)
    val servletRequest: HttpServletRequest = mock()
    val request = listOf("GECAL", "HP", "13", "123", "07", "MK09", "name", "9002392323", "", "P", "RETAKE_PHOTO", "Y", "N", "Y",
            "23042014", "143045", "100.0", "19", "11.0", "10.0")
    describe("LoanApplicationController") {
        val traceId = "traceId"
        val httpHeaders = HttpHeaders()
        httpHeaders.add("X-Amzn-Trace-Id", traceId)
        Mockito.`when`(servletRequest.getHeader("X-Amzn-Trace-Id")).thenReturn(traceId)
        val httpEntity = HttpEntity(AfsFeedbackBuilder.parseAndBuild(request), httpHeaders)
        val url = jetConfiguration.url + "/api/afs-feedback"

        loanApplicationController.forwardLoanInfo(servletRequest, request)

        it("call restTemplate with correct params") {
            verify(restTemplate).postForEntity(url, httpEntity, AFSFeedback::class.java)
        }
    }
})