package com.jet.service.adventure.filters

import org.slf4j.MDC
import org.springframework.core.Ordered
import org.springframework.core.annotation.Order
import org.springframework.stereotype.Component
import org.springframework.web.filter.OncePerRequestFilter
import java.io.IOException
import java.util.UUID
import javax.servlet.FilterChain
import javax.servlet.ServletException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
@Order(Ordered.LOWEST_PRECEDENCE)
class MDCRequestFilter : OncePerRequestFilter() {

    @Throws(ServletException::class, IOException::class)
    override fun doFilterInternal(request: HttpServletRequest, response: HttpServletResponse, filterChain: FilterChain) {
        try {
            listOf("X-Amzn-Trace-Id").forEach { addHeaderToMdc(request, it) }

            filterChain.doFilter(request, response)
        } finally {
            MDC.clear()
        }
    }

    private fun addHeaderToMdc(request: HttpServletRequest, headerName: String) {
        val value = request.getHeader(headerName) ?: UUID.randomUUID().toString()
        MDC.put(headerName.toLowerCase(), value)
    }
}