package com.jet.service.adventure.parsers

import com.jet.service.adventure.model.Application
import com.jet.service.adventure.model.AFSFeedback
import com.jet.service.adventure.model.TriggerCode
import com.jet.service.adventure.model.Verification
import com.jet.service.adventure.model.MarketRepresentative
import com.jet.service.adventure.model.PreApprove
import com.jet.service.adventure.util.toApplicationStatus
import com.jet.service.adventure.util.toApplicationVerificationStatus
import com.jet.service.adventure.util.toBoolean
import com.jet.service.adventure.util.toZonedDateTime
import com.jet.service.adventure.util.toProductType
import com.jet.service.adventure.util.toTriggerCode

class AfsFeedbackBuilder {
    companion object {
        private const val COMPANY = "COMPANY"
        private const val PRODUCT = "PRODUCT"
        private const val BRANCH = "BRANCH"
        private const val LOAN_ID = "LOAN_ID"
        private const val TRIGGER_CODE = "TRIGGER_CODE"
        private const val MR_CODE = "MR_CODE"
        private const val MR_NAME = "MR_NAME"
        private const val MR_MOBILE = "MR_MOBILE"
        private const val APP_STATUS = "APP_STATUS"
        private const val APP_VERIFY_STATUS = "APP_VERIFY_STATUS"
        private const val APP_VERIFY_CODE = "APP_VERIFY_CODE"
        private const val CITIZEN_ID_STATUS = "CITIZEN_ID_STATUS"
        private const val PAYSLIP_STATUS = "PAYSLIP_STATUS"
        private const val STATEMENT_OR_EMPLOYEE_CARD_STATUS = "STATEMENT_OR_EMPLOYEE_CARD_STATUS"
        private const val STATUS_DATE = "STATUS_DATE"
        private const val STATUS_TIME = "STATUS_TIME"
        private const val APPROVAL_AMOUNT = "APPROVAL_AMOUNT"
        private const val APPROVAL_TERM = "APPROVAL_TERM"
        private const val DOWN_PAYMENT = "DOWN_PAYMENT"
        private const val MAX_INSTALLMENT = "MAX_INSTALLMENT"

        private val fieldNameArray = arrayOf(COMPANY, PRODUCT, BRANCH, LOAN_ID, TRIGGER_CODE,
            MR_CODE, MR_NAME, MR_MOBILE, APP_STATUS, APP_VERIFY_STATUS, APP_VERIFY_CODE,
            CITIZEN_ID_STATUS, PAYSLIP_STATUS, STATEMENT_OR_EMPLOYEE_CARD_STATUS, STATUS_DATE, STATUS_TIME,
            APPROVAL_AMOUNT, APPROVAL_TERM, DOWN_PAYMENT, MAX_INSTALLMENT)

        fun parseAndBuild(afsData: List<String>): AFSFeedback {
            val fieldData = convertListToMap(afsData)
            val triggerCode = fieldData[TRIGGER_CODE].toTriggerCode()
            val application = getApplicationDetails(fieldData)

            return AFSFeedback(triggerCode, application)
        }

        private fun getApplicationDetails(fieldData: Map<String, String>): Application {
            val loanId = fieldData[LOAN_ID].toString()
            val company = fieldData[COMPANY].toString()
            val product = fieldData[PRODUCT].toProductType()
            val branch = fieldData[BRANCH].toString()
            val applicationStatus = fieldData[APP_STATUS].toApplicationStatus()
            val marketRepresentative = getMRDetails(fieldData)
            val triggerCode = fieldData[TRIGGER_CODE].toTriggerCode()
            val verification = getApplicationVerificationDetails(fieldData, triggerCode)
            val statusDate = fieldData[STATUS_DATE].toString()
            val statusTime = fieldData[STATUS_TIME].toString()
            val statusDateTimeInISO8601Format = statusDate.toZonedDateTime(statusTime)
            val preApprove = getPreApprovalDetails(fieldData)

            return Application(loanId, company, product, branch, applicationStatus, marketRepresentative, verification, statusDateTimeInISO8601Format, preApprove)
        }

        private fun getApplicationVerificationDetails(fieldData: Map<String, String>, triggerCode: TriggerCode): Verification {
            val verificationReasonCode = fieldData[APP_VERIFY_CODE].toString()
            val verificationStatus = fieldData[APP_VERIFY_STATUS].toApplicationVerificationStatus(triggerCode, verificationReasonCode)
            val citizenIdStatus = fieldData[CITIZEN_ID_STATUS].toBoolean()
            val paySlipStatus = fieldData[PAYSLIP_STATUS].toBoolean()
            val statementOrEmployeeCardStatus = fieldData[STATEMENT_OR_EMPLOYEE_CARD_STATUS].toBoolean()

            return Verification(verificationStatus, verificationReasonCode, citizenIdStatus, paySlipStatus, statementOrEmployeeCardStatus)
        }

        private fun getMRDetails(fieldData: Map<String, String>): MarketRepresentative {
            val code = fieldData[MR_CODE].toString()
            val name = fieldData[MR_NAME].toString()
            val mobileNumber = fieldData[MR_MOBILE].toString()

            return MarketRepresentative(mobileNumber, code, name)
        }

        private fun getPreApprovalDetails(fieldData: Map<String, String>): PreApprove {
            val preApproveAmount = fieldData[APPROVAL_AMOUNT]?.toDoubleOrNull()
            val preApproveTerm = fieldData[APPROVAL_TERM]?.toIntOrNull()
            val preApprovePercentDown = fieldData[DOWN_PAYMENT]?.toDoubleOrNull()
            val preApproveInstallment = fieldData[MAX_INSTALLMENT]?.toDoubleOrNull()

            return PreApprove(preApproveAmount, preApproveTerm, preApprovePercentDown, preApproveInstallment)
        }

        private fun convertListToMap(feedback: List<String>): Map<String, String> {
            var map = HashMap<String, String>()
            feedback.forEachIndexed { index, value -> map[fieldNameArray[index]] = value }
            return map
        }
    }
}