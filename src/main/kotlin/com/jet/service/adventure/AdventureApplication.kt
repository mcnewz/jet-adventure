package com.jet.service.adventure

import com.jet.service.adventure.configuration.JetConfigurationProperties
import com.jet.service.adventure.ewi.model.EwiConfigurationProperties
import com.jet.service.adventure.idoc.model.IDocConfigurationProperties
import com.jet.service.adventure.ifu.model.IFuConfigurationProperties
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication

@SpringBootApplication
@EnableConfigurationProperties(
    EwiConfigurationProperties::class,
    IFuConfigurationProperties::class,
    IDocConfigurationProperties::class,
    JetConfigurationProperties::class
)
class AdventureApplication

fun main(args: Array<String>) {
    @Suppress("SpreadOperator")
    runApplication<AdventureApplication>(*args)
}