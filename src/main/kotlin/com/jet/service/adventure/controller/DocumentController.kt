package com.jet.service.adventure.controller

import com.jet.service.adventure.biztech.Entity
import com.jet.service.adventure.model.DocumentRequest
import com.jet.service.adventure.model.ProductType
import com.jet.service.adventure.service.DocumentService
import com.jet.service.adventure.service.ImageService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.multipart.MultipartFile

data class DocumentFileResponse(val filePath: String)
data class DocumentResponse(val applicationNo: String, val documentSetId: String)

@RestController
@RequestMapping("/document")
class DocumentController(
    val imageService: ImageService,
    val documentService: DocumentService
) {
    val logger: Logger = LoggerFactory.getLogger(this::class.java)

    @PostMapping("")
    fun submit(@RequestBody request: DocumentRequest): ResponseEntity<DocumentResponse> {
        logger.info("Request : $request")
        val documentSetId = documentService.submit(request)
        val response = DocumentResponse(request.applicationNo, documentSetId)
        logger.info("Response: $response")
        return ResponseEntity.status(HttpStatus.CREATED).body(response)
    }

    @PostMapping("/file")
    fun upload(@RequestParam productType: ProductType, @RequestParam file: MultipartFile): ResponseEntity<DocumentFileResponse> {
        logger.info("ProductType: $productType")
        val filePath = imageService.upload(file, Entity.fromProductCode(productType.toProductCode()))
        val documentResponse = DocumentFileResponse(filePath)
        logger.info("Response: $documentResponse")
        return ResponseEntity.status(HttpStatus.CREATED).body(documentResponse)
    }

    @PostMapping("/warmup")
    fun warmup(): ResponseEntity<DocumentFileResponse> {
        logger.info("Warming up service")
        val filePath = imageService.warmup()
        val documentResponse = DocumentFileResponse(filePath)
        logger.info("Response: $documentResponse")
        return ResponseEntity.status(HttpStatus.CREATED).body(documentResponse)
    }
}