package com.jet.service.adventure.controller

import com.jet.service.adventure.configuration.JetConfigurationProperties
import com.jet.service.adventure.model.AFSFeedback
import com.jet.service.adventure.model.LoanApplicationDocumentAppendRequest
import com.jet.service.adventure.model.LoanApplicationDocumentAppendResponse
import com.jet.service.adventure.model.LoanApplicationRequest
import com.jet.service.adventure.model.LoanApplicationResponse
import com.jet.service.adventure.parsers.AfsFeedbackBuilder
import com.jet.service.adventure.service.AwsSecretsService
import com.jet.service.adventure.service.LoanApplicationService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.client.support.BasicAuthorizationInterceptor
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.client.RestTemplate
import java.util.UUID
import javax.servlet.http.HttpServletRequest
import javax.validation.Valid

@RestController
@RequestMapping("/loan-application")
class LoanApplicationController(
    val loanApplicationService: LoanApplicationService,
    val jetConfiguration: JetConfigurationProperties,
    val restTemplateWithProxy: RestTemplate,
    val awsSecretsService: AwsSecretsService
) {

    val logger: Logger = LoggerFactory.getLogger(LoanApplicationController::class.java)

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun create(@Valid @RequestBody request: LoanApplicationRequest): LoanApplicationResponse {
        logger.info("Received Request: $request")
        return loanApplicationService.create(request)
    }

    @PostMapping("/append")
    @ResponseStatus(HttpStatus.OK)
    fun append(@RequestBody request: LoanApplicationDocumentAppendRequest): LoanApplicationDocumentAppendResponse {
        logger.info("Received Request: $request")
        return loanApplicationService.setAppend(request)
    }

    @PostMapping("/feedback")
    @ResponseStatus(HttpStatus.CREATED)
    fun forwardLoanInfo(servletRequest: HttpServletRequest, @RequestBody request: List<String>) {
        logger.info("Received Request: $request")
        val feedback = AfsFeedbackBuilder.parseAndBuild(request)
        val headers = HttpHeaders()
        val traceId = servletRequest.getHeader("X-Amzn-Trace-Id") ?: "AFS_" + UUID.randomUUID().toString()
        headers.set("X-Amzn-Trace-Id", traceId)
        val url = jetConfiguration.url + "/api/afs-feedback"

        val (username, password) = awsSecretsService.getAfsFeedbackEndpointCredentials()
        val basicAuth = BasicAuthorizationInterceptor(username, password)

        restTemplateWithProxy.interceptors = restTemplateWithProxy
                .interceptors
                .filter { it !is BasicAuthorizationInterceptor }
                .plus(basicAuth)

        restTemplateWithProxy.postForEntity(url, HttpEntity(feedback, headers), AFSFeedback::class.java)
    }
}