package com.jet.service.adventure.ewi.service

import EWI.ewiPCJET.schema.jetpcrequestservice.JetpcrequestserviceRequestContentType
import EWI.ewiPCJET.schema.jetpcrequestservice.JetpcrequestserviceRequestType
import EWI.ewiPCJET.schema.jetpcrequestservice.JetpcrequestserviceResponseType
import EWI.ewiPCJET.webservice.jetpcrequestservice.JetpcrequestservicePortType
import EWI.ewiPCJET.webservice.jetpcrequestservice.JetpcrequestservicePortTypeProxy
import com.jet.service.adventure.ewi.exception.EwiResponseException
import com.jet.service.adventure.ewi.model.EwiConfigurationProperties
import com.jet.service.adventure.model.ProductType
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Component
import java.util.UUID

@Component
@Profile("EWI_LIVE_ACCESS")
class EwiClientImpl(
    val ewiConfig: EwiConfigurationProperties
) : EwiClient {

    val logger: Logger = LoggerFactory.getLogger(EwiClientImpl::class.java)
    private val proxy = JetpcrequestservicePortTypeProxy()

    private fun getPort(): JetpcrequestservicePortType {
        proxy.endpoint = ewiConfig.endpoint
        return proxy.jetpcrequestservicePortType
    }

    private fun buildRequest(programName: String, productType: ProductType, transactionId: String, pDataIn: String): JetpcrequestserviceRequestType {
        val request = JetpcrequestserviceRequestType()
        val requestContent = JetpcrequestserviceRequestContentType()

        val entity = when (productType) {
            ProductType.FOUR_WHEELER -> ewiConfig.bay
            ProductType.TWO_WHEELER -> ewiConfig.aycal
        }

        val token = "JET$transactionId"

        request.username = entity.username
        request.ewitoken = token
        request.usergroup = entity.userGroup
        request.workstation = entity.workstation
        requestContent.afsUser = entity.afsUser

        request.content = requestContent

        requestContent.programName = programName
        requestContent.company = ""
        requestContent.product = ""
        requestContent.branch = ""
        requestContent.serviceClientCode = ""

        requestContent.pDataIn = pDataIn

        return request
    }

    private fun throwOnFailure(response: JetpcrequestserviceResponseType, requestId: String) {
        val returnCode = response.returncode
        val returnMessage = response.runmessage
        if (returnCode != "EWI-0000I") {
            logger.error("[$requestId] EWI Error[$returnCode]: $returnMessage")
            throw EwiResponseException(returnMessage, returnCode)
        }
    }

    private fun requestId(transactionId: String): String {
        if (transactionId.isNotEmpty()) {
            return transactionId
        }
        return UUID.randomUUID().toString()
    }

    override fun send(programName: String, transactionId: String, productType: ProductType, pDataIn: String): String {
        val requestId = requestId(transactionId)
        val request = buildRequest(programName, productType, transactionId, pDataIn)
        val port = getPort()
        lateinit var response: JetpcrequestserviceResponseType
        logger.info("[$requestId] Sending Request to EWI...")
        try {
            response = port.jetpcrequestserviceContainer(request)
        } catch (e: Throwable) {
            logger.error("[$requestId] EWI Connection Error", e)
            throw EwiResponseException("[$requestId] EWI Connection Error", "")
        }
        throwOnFailure(response, requestId)
        val pDataOut = response.content.pDataOut
        logger.info("[$requestId] Received Response from EWI.")
        return pDataOut
    }
}