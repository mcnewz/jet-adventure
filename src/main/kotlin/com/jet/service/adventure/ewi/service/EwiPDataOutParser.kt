package com.jet.service.adventure.ewi.service

import com.jet.service.adventure.ewi.model.EwiResponse

class EwiPDataOutParser (
    val responseCodePosition: Int,
    val responseCodeLength: Int,
    val dataPosition: Int
) {
    fun parse(pDataOut: String): EwiResponse {
        val responseCodeOffset = responseCodePosition - 1
        val dataOffset = dataPosition - 1
        val code = pDataOut.substring(responseCodeOffset, responseCodeOffset + responseCodeLength).trim()
        val data = pDataOut.substring(dataOffset).trim()
        return EwiResponse(code, data)
    }
}