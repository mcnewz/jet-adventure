package com.jet.service.adventure.ewi.model

data class EwiResponse (
    val header: String,
    val data: String
)