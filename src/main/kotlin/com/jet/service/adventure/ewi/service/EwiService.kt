package com.jet.service.adventure.ewi.service

import com.jet.service.adventure.ewi.exception.AfsResponseException
import com.jet.service.adventure.ewi.exception.EwiServiceValidationException
import com.jet.service.adventure.ewi.model.CreateLoanApplicationParam
import com.jet.service.adventure.ewi.model.CreateLoanApplicationResponseData
import com.jet.service.adventure.ewi.model.CreateLoanApplicationResult
import com.jet.service.adventure.ewi.model.EwiResponse
import com.jet.service.adventure.ewi.model.EwiResponseHeader
import com.jet.service.adventure.ewi.model.MarkFlagParam
import com.jet.service.adventure.model.ProductType
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service

@Service
class EwiService(
    val ewiClient: EwiClient
) {
    val logger: Logger = LoggerFactory.getLogger(EwiService::class.java)

    fun createLoanApplication (param: CreateLoanApplicationParam, productType: ProductType): CreateLoanApplicationResult {
        val ewiPDataOutParser = EwiPDataOutParser(4951, 200, 5151)
        logger.info("Submitting loan application $param")
        val pDataIn = serializeParam(param)
        val pDataOut = ewiClient.send("JT1I001", param.jetNumber, productType, pDataIn)
        logger.info("Done!")
        val ewiResponse = ewiPDataOutParser.parse(pDataOut)
        throwOnFailure(ewiResponse)
        return CreateLoanApplicationResponseData.parse(ewiResponse.data)
    }

    fun markFlag (param: MarkFlagParam, productType: ProductType) {
        val ewiPDataOutParser = EwiPDataOutParser(4951, 200, 5151)
        logger.info("Mark Flag $param")
        val pDataIn = serializeParam(param)
        val pDataOut = ewiClient.send("JT1I002", param.jetNumber, productType, pDataIn)
        logger.info("Done!")
        val ewiResponse = ewiPDataOutParser.parse(pDataOut)
        throwOnFailure(ewiResponse)
    }

    private fun throwOnFailure(ewiResponse: EwiResponse) {
        val responseHeader = EwiResponseHeader.parse(ewiResponse.header)
        if (responseHeader.code != "0000") {
            throw AfsResponseException(responseHeader.description, responseHeader.code)
        }
    }

    private fun serializeParam(param: Any): String {
        val serializer = AfsSerializer()
        try {
            return serializer.serialize(param)
        } catch (e: Throwable) {
            throw EwiServiceValidationException("Unable to serialize param: $param")
        }
    }
}