package com.jet.service.adventure.ewi.exception

class AfsResponseException (
    override val message: String,
    val code: String
) : Exception(message)