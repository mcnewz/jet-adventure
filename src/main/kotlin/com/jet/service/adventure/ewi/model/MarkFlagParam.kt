package com.jet.service.adventure.ewi.model

import com.jet.service.adventure.ewi.service.AfsField

data class MarkFlagParam (
    @AfsField val company: String,
    @AfsField val productCode: String,
    @AfsField val branchCode: String,
    @AfsField val applicationNo: String,
    @AfsField val flag: String,
    @AfsField val color: String,
    val jetNumber: String
)