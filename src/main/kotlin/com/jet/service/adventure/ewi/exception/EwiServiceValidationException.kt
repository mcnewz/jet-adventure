package com.jet.service.adventure.ewi.exception

class EwiServiceValidationException(override val message: String?) : Exception(message)