package com.jet.service.adventure.ewi.service

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.FIELD)
annotation class AfsField(val length: Int = 0, val pattern: String = "")