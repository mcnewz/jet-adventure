package com.jet.service.adventure.ewi.service

import java.lang.reflect.Field
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

class AfsSerializer(private val delimiter: CharSequence = "|", private val padChar: Char = ' ') {

    private val annotationClass = AfsField::class.java

    fun serialize(obj: Any): String {
        val clazz = obj.javaClass
        val annotatedFields = clazz.declaredFields.filter { it.isAnnotationPresent(annotationClass) }
        val serializedFields = annotatedFields.map { serializeField(it, obj) }
        return serializedFields.joinToString(delimiter) + delimiter
    }

    private fun serializeField(field: Field, obj: Any): String {
        field.isAccessible = true
        val value = field.get(obj)
        val pattern = field.getAnnotation(annotationClass).pattern
        val length = field.getAnnotation(annotationClass).length
        var result = value?.toString() ?: ""
        if (pattern != "") {
            val formatter = DateTimeFormatter.ofPattern(pattern)
            result = when (value) {
                is LocalDate -> value.format(formatter)
                is LocalTime -> value.format(formatter)
                is LocalDateTime -> value.format(formatter)
                is ZonedDateTime -> value.format(formatter)
                else -> result
            }
        }
        return result.padEnd(length, padChar)
    }
}