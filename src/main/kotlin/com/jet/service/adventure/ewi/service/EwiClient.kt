package com.jet.service.adventure.ewi.service

import com.jet.service.adventure.ewi.exception.EwiResponseException
import com.jet.service.adventure.model.ProductType

interface EwiClient {
    @Throws(EwiResponseException::class)
    fun send(programName: String, transactionId: String, productType: ProductType, pDataIn: String): String
}