package com.jet.service.adventure.ewi.model

data class CreateLoanApplicationResponseData (
    val company: String,
    val productCode: String,
    val branchCode: String,
    val loanId: String,
    val jetNumber: String,
    val regionCode: String,
    val hubId: String
) {
    companion object {
        fun parse(s: String): CreateLoanApplicationResponseData {
            val values = s.split("|")
            return CreateLoanApplicationResponseData(values[0], values[1], values[2], values[3], values[4], values[5], values[6])
        }
    }
}