package com.jet.service.adventure.ewi.model

import com.jet.service.adventure.ewi.service.AfsField
import com.jet.service.adventure.util.MaskedField
import com.jet.service.adventure.util.dataClassToString

data class CreateLoanApplicationParam (

    @AfsField val thTitle: String,
    @AfsField val thFirstName: String,
    @AfsField val thSurname: String,
    @AfsField val enTitle: String,
    @AfsField val enFirstName: String,
    @AfsField val enSurname: String,
    @AfsField val birthDate: String,
    @AfsField val customerType: String,
    @AfsField @MaskedField val idCard: String,
    @AfsField val idCardExpire: String,
    @AfsField val cardType: String,
    @AfsField val marital: String,
    @AfsField val address1: String,
    @AfsField val address2: String,
    @AfsField val address3: String,
    @AfsField val zipCode: String,
    @AfsField val campaignCode: String,
    @AfsField val ncbType: String,
    @AfsField val income: String,
    @AfsField val product: String,
    @AfsField val occupationCode: String,
    @AfsField @MaskedField val mobileNo: String,
    @AfsField val marketingDate: String,
    @AfsField val marketingTime: String,
    @AfsField val branchCode: String,
    @AfsField val mrCode: String,
    @AfsField val dealerCode: String,
    @AfsField val telesalesCode: String,
    @AfsField val riskQuestion1: String,
    @AfsField val riskQuestion2: String,
    @AfsField val riskQuestion3: String,
    @AfsField val riskQuestion4: String,
    @AfsField val jetNumber: String,
    @AfsField val other: String

) {
    override fun toString(): String {
        return dataClassToString(this)
    }
}