package com.jet.service.adventure.ewi.exception

class EwiResponseException(
    override val message: String,
    val code: String
) : Exception(message)