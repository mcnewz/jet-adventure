package com.jet.service.adventure.ewi.model

data class EwiResponseHeader (
    val code: String,
    val description: String
) {
    companion object {
        fun parse(s: String): EwiResponseHeader {
            val values = s.split("-")
            if (values.size < 2) {
                return EwiResponseHeader("", values[0])
            }
            return EwiResponseHeader(values[0], values.subList(1, values.size).joinToString("-"))
        }
    }
}