package com.jet.service.adventure.ewi.model

import org.springframework.boot.context.properties.ConfigurationProperties

data class EntityConfiguration(
    var username: String?,
    var userGroup: String?,
    var workstation: String?,
    var afsUser: String?
)

@ConfigurationProperties("ewi")
data class EwiConfigurationProperties(
    var endpoint: String?,
    var bay: EntityConfiguration = EntityConfiguration(null, null, null, null),
    var aycal: EntityConfiguration = EntityConfiguration(null, null, null, null)
)