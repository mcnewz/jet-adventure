package com.jet.service.adventure.biztech

import com.minimal.base.webservice.invoker.BizTeCHWeBServiceInvoKer

class WebService {
    companion object {
        inline fun <reified T> build(endpoint: String, timeout: Int): T {
            val invoker = BizTeCHWeBServiceInvoKer()
            invoker.serviceEndPoint = endpoint
            invoker.connectionTimeout = timeout.toString()
            return invoker.newInstance(T::class.java) as T
        }
    }
}