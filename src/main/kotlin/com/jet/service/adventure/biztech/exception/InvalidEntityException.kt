package com.jet.service.adventure.biztech.exception

class InvalidEntityException(message: String?) : RuntimeException(message)