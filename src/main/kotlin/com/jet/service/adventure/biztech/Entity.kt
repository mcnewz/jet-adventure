package com.jet.service.adventure.biztech

import com.jet.service.adventure.biztech.exception.InvalidEntityException

enum class Entity {
    BAY, AYCAL;

    companion object {
        fun fromProductCode(productCode: String): Entity {
            return when (productCode) {
                "HP" -> BAY
                "MC", "BB" -> AYCAL
                else -> throw InvalidEntityException("Invalid productCode $productCode.")
            }
        }
    }
}