package com.jet.service.adventure.configuration

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.client.SimpleClientHttpRequestFactory
import org.springframework.web.client.RestTemplate
import java.net.InetSocketAddress
import java.net.Proxy

@Configuration
class JetConfiguration(val config: JetConfigurationProperties) {

    @Bean
    fun restTemplate(): RestTemplate {
        return RestTemplate()
    }

    @Bean
    fun restTemplateWithProxy(): RestTemplate {
        val requestFactory = SimpleClientHttpRequestFactory()
        val proxy = Proxy(Proxy.Type.HTTP, InetSocketAddress(config.proxyHost, config.proxyPort))
        requestFactory.setProxy(proxy)
        return RestTemplate(requestFactory)
    }
}