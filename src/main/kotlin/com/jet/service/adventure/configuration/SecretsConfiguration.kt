package com.jet.service.adventure.configuration

import com.amazonaws.ClientConfiguration
import com.amazonaws.regions.Regions
import com.amazonaws.services.secretsmanager.AWSSecretsManagerClientBuilder
import com.jet.service.adventure.service.AwsSecretsService
import com.jet.service.adventure.service.LiveAwsSecretsService
import com.jet.service.adventure.service.StubAwsSecretsService
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile

@Configuration
class SecretsConfiguration {
    @Bean
    @Profile("AWS_SECRET_LIVE_ACCESS")
    fun liveAwsSecretsService(
        config: JetConfigurationProperties,
        @Value("\${aws-secrets-names.afs-credentials:}") afsFeedbackSecretId: String
    ): AwsSecretsService {
        val clientConfig = ClientConfiguration()
                .withProxyHost(config.proxyHost)
                .withProxyPort(config.proxyPort)

        val secretManager = AWSSecretsManagerClientBuilder.standard()
                .withRegion(Regions.AP_SOUTHEAST_1)
                .withClientConfiguration(clientConfig)
                .build()

        return LiveAwsSecretsService(secretManager, afsFeedbackSecretId)
    }

    @Bean
    @Profile("AWS_SECRET_STUB_ACCESS")
    fun stubAwsSecretsService(): AwsSecretsService =
        StubAwsSecretsService()
}