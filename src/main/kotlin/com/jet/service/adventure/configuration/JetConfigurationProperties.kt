package com.jet.service.adventure.configuration

import org.springframework.boot.context.properties.ConfigurationProperties

@ConfigurationProperties("jet")
data class JetConfigurationProperties(
    var url: String = "",
    var proxyHost: String = "",
    var proxyPort: Int = 80
)