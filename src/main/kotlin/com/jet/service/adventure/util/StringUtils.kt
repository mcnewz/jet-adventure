package com.jet.service.adventure.util

import com.jet.service.adventure.exceptions.AfsFeedbackParsingException
import com.jet.service.adventure.model.ApplicationStatus
import com.jet.service.adventure.model.ProductType
import com.jet.service.adventure.model.TriggerCode
import com.jet.service.adventure.model.VerificationStatus
import java.time.ZoneId
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

fun String?.toBoolean(): Boolean {
    val upperCaseOfBoolean = this?.toUpperCase() ?: ""
    return when (upperCaseOfBoolean) {
        "Y" -> true
        "N" -> false
        "TRUE" -> true
        "FALSE" -> false
        else -> false
    }
}

fun String?.toProductType(): ProductType {
    val productCode = this?.toUpperCase() ?: ""
    return when (productCode) {
        "HP" -> ProductType.FOUR_WHEELER
        "MC" -> ProductType.TWO_WHEELER
        else -> throw AfsFeedbackParsingException("Invalid Product Code '$this'")
    }
}

fun String?.toTriggerCode(): TriggerCode {
    return when (this) {
        "01" -> TriggerCode.UPDATE_INFORMATION
        "02" -> TriggerCode.REJECT
        "03" -> TriggerCode.PRE_APPROVED
        "04" -> TriggerCode.HARD_CASE
        "05" -> TriggerCode.CREDIT_LINE_EXPIRED
        "06" -> TriggerCode.LOAN_UTILIZED
        "07" -> TriggerCode.MANUAL_ASSIGN_MR
        "08" -> TriggerCode.RETAKE_PHOTO
        "09" -> TriggerCode.CANCEL
        "10" -> TriggerCode.OTHERS
        else -> throw AfsFeedbackParsingException("Invalid Trigger Code '$this'")
    }
}

fun String?.toApplicationStatus(): ApplicationStatus {
    return when (this) {
        "A" -> ApplicationStatus.ACTIVE
        "C" -> ApplicationStatus.CONTRACTED
        "D" -> ApplicationStatus.AUTO_CANCEL
        "" -> ApplicationStatus.NOT_ACTIVE
        else -> throw AfsFeedbackParsingException("Invalid Application Status '$this'")
    }
}

fun String?.toApplicationVerificationStatus(triggerCode: TriggerCode, reasonCode: String): VerificationStatus {
    return when (this) {
        "P" ->
            when (triggerCode) {
                TriggerCode.PRE_APPROVED -> VerificationStatus.PRE_APPROVED
                TriggerCode.HARD_CASE -> VerificationStatus.HARD_CASE
                TriggerCode.CANCEL -> VerificationStatus.AUTOMATIC_CANCEL
                else ->
                    when (reasonCode) {
                    "81" -> VerificationStatus.PRE_APPROVED
                        else -> VerificationStatus.PENDING
                }
            }
        "R" -> VerificationStatus.REJECT
        "D" -> VerificationStatus.MANUAL_CANCEL
        "Y" -> VerificationStatus.UTILIZE
        else -> throw AfsFeedbackParsingException("Invalid Application Verification Status '$this'")
    }
}

fun String?.toZonedDateTime(time: String): ZonedDateTime {
    val formatter = DateTimeFormatter.ofPattern("ddMMyyyyHHmmss ZZ")
    return ZonedDateTime.parse("${this}$time +0700", formatter).minusYears(543)
            .withZoneSameInstant(ZoneId.of("Asia/Bangkok"))
}