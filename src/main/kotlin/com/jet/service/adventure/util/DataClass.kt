package com.jet.service.adventure.util

fun dataClassToString(obj: Any): String {
    val annotationClass = MaskedField::class.java
    val clazz = obj.javaClass
    val name = clazz.simpleName
    val fieldStrings = mutableListOf<String>()
    clazz.declaredFields.forEach {
        it.isAccessible = true
        var value = it.get(obj).toString()
        if (it.isAnnotationPresent(annotationClass)) {
            val annotation = it.getAnnotation(annotationClass)
            value = mask(value, annotation.maskChar, annotation.revealFront, annotation.revealBack)
        }
        fieldStrings.add("${it.name}=$value")
    }

    val joinedFields = fieldStrings.joinToString(", ")
    var result = "$name($joinedFields)"
    return result
}

fun mask(value: String, maskChar: Char, revealFront: Int, revealBack: Int): String {
    val length = value.length
    val start = revealFront
    val end = length - revealBack
    if (start >= end) {
        return value
    }
    val replaceLength = end - start
    val replaceString = maskChar.toString().repeat(replaceLength)
    return value.replaceRange(start, end, replaceString)
}