package com.jet.service.adventure.util

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.FIELD)
annotation class MaskedField(val maskChar: Char = 'X', val revealFront: Int = 2, val revealBack: Int = 2)