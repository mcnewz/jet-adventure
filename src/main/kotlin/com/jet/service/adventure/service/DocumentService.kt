package com.jet.service.adventure.service

import com.jet.service.adventure.idoc.IDocService
import com.jet.service.adventure.idoc.model.IDocAppendParam
import com.jet.service.adventure.idoc.model.IDocCreateParam
import com.jet.service.adventure.idoc.model.IDocCustomerInfo
import com.jet.service.adventure.model.DocumentRequest
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service

@Service
class DocumentService(val iDocService: IDocService) {

    val logger: Logger = LoggerFactory.getLogger(this::class.java)

    fun submit(request: DocumentRequest): String {
        return tryAppendWithApplicationNo(request) ?: tryAppendWithDocumentSetId(request) ?: create(request)
    }

    private fun create(request: DocumentRequest): String {
        logger.debug("create new documentSet - params : $request")
        val iDocCreateParam = IDocCreateParam(
                request.customerFirstName,
                request.customerLastName,
                request.customerBirthDate,
                request.customerCitizenId,
                request.applicationNo,
                request.branchId,
                request.hubId,
                request.productType.toProductCode(),
                request.imageFilePaths)
        return iDocService.create(iDocCreateParam)
    }

    private fun append(documentSetId: String, request: DocumentRequest): String {
        val iDocAppendParam = IDocAppendParam(
                documentSetId,
                request.productType.toProductCode(),
                request.hubId,
                request.branchId,
                request.imageFilePaths)
        return iDocService.append(iDocAppendParam)
    }

    private fun tryAppendWithApplicationNo(request: DocumentRequest): String? {
        val documentSetId = iDocService.getDocumentSetIdByApplicationNo(
                request.productType.toProductCode(), applicationNo(request)) ?: return null
        logger.debug("append by applicationNo - params : $request")
        return append(documentSetId, request)
    }

    private fun tryAppendWithDocumentSetId(request: DocumentRequest): String? {
        val documentSetId = request.documentSetId ?: return null
        val documentSet = iDocService.getDocumentSetById(request.productType.toProductCode(), documentSetId) ?: return null
        iDocService.unResetData(request.productType.toProductCode(), documentSet, applicationNo(request), IDocCustomerInfo(
            request.customerFirstName,
            request.customerLastName,
            request.customerBirthDate,
            request.customerCitizenId
        ))
        logger.debug("append by documentSetId - params : $request")
        return append(documentSetId, request)
    }

    private fun applicationNo(request: DocumentRequest): String = "${request.branchId}${request.applicationNo}"
}