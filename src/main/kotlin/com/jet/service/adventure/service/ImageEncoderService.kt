package com.jet.service.adventure.service

import org.springframework.web.multipart.MultipartFile
import java.util.Base64

class ImageEncoderService {
    companion object {
        fun encodeToBase64 (file: MultipartFile): String {
            return Base64.getEncoder().encodeToString(file.bytes)
        }
    }
}