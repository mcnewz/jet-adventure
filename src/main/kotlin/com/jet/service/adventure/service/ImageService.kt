package com.jet.service.adventure.service

import com.jet.service.adventure.biztech.Entity
import com.jet.service.adventure.ifu.IFuClient
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile
import java.util.UUID

@Service
class ImageService(
    val iFuClient: IFuClient
) {
    val logger: Logger = LoggerFactory.getLogger(ImageService::class.java)

    fun upload(file: MultipartFile, entity: Entity): String {
        val filename = "${UUID.randomUUID()}.jpg"
        val base64Str = ImageEncoderService.encodeToBase64(file)
        logger.info("Uploading: $filename")
        iFuClient.upload(entity, filename, base64Str)
        logger.info("Uploading Done: $filename")
        val imagePath = iFuClient.getRepositoryPath(entity)
        return "$imagePath\\$filename"
    }

    fun warmup(): String {
        val filename = "warmup.jpg"
        val base64Str = "FOO"
        iFuClient.upload(Entity.AYCAL, filename, base64Str)
        return filename
    }
}