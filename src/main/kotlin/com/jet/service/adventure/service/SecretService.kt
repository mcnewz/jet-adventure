package com.jet.service.adventure.service

import com.amazonaws.services.secretsmanager.AWSSecretsManager
import com.amazonaws.services.secretsmanager.model.GetSecretValueRequest
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.slf4j.LoggerFactory
import java.time.LocalDateTime

data class UsernamePassword(val username: String, val password: String)

interface AwsSecretsService {
    fun getAfsFeedbackEndpointCredentials(): UsernamePassword
}

data class CachedSecret<out T>(val secret: T, val timestamp: LocalDateTime)
data class CachedSecretHolder<T>(val name: String, var cachedSecret: CachedSecret<T>? = null)

class LiveAwsSecretsService(
    private val secretManager: AWSSecretsManager,
    private val afsFeedbackSecretId: String
) : AwsSecretsService {

    companion object {
        val LOGGER = LoggerFactory.getLogger(LiveAwsSecretsService::class.java)
        const val CACHE_TIMEOUT: Long = 5
    }

    private val afsFeedbackUsernamePasswordHolder: CachedSecretHolder<UsernamePassword> =
            CachedSecretHolder("afsFeedbackUsernamePassword")

    override fun getAfsFeedbackEndpointCredentials() = fetchWhenTimeout(afsFeedbackUsernamePasswordHolder) {
        getSecretJson(afsFeedbackSecretId)
    }

    fun getSecretString(secretId: String): String {
        val request = GetSecretValueRequest().withSecretId(secretId)
        val result = secretManager.getSecretValue(request)
        return result.secretString
    }

    inline fun <reified T> LiveAwsSecretsService.getSecretJson(secretId: String): T {
        val credentialsString = getSecretString(secretId)
        val objectMapper = jacksonObjectMapper()
        return objectMapper.readValue(credentialsString, T::class.java)
    }

    inline fun <reified T> fetchWhenTimeout(cachedSecretHolder: CachedSecretHolder<T>, fetch: () -> T): T {
        val now = LocalDateTime.now()
        val isSomeSecretCached = cachedSecretHolder.cachedSecret != null

        val isNewSecretNeeded = if (isSomeSecretCached) {
            val lastFetched = cachedSecretHolder.cachedSecret!!.timestamp
            now.isAfter(lastFetched.plusMinutes(CACHE_TIMEOUT))
        } else {
            true
        }

        return if (isNewSecretNeeded) {
            cachedSecretHolder.cachedSecret = CachedSecret(fetch(), now)
            LOGGER.info("got new secret for ${cachedSecretHolder.name} at $now")
            cachedSecretHolder.cachedSecret!!.secret
        } else {
            LOGGER.info("using old secret for ${cachedSecretHolder.name} fetched at ${cachedSecretHolder.cachedSecret!!.timestamp}")
            cachedSecretHolder.cachedSecret!!.secret
        }
    }
}

class StubAwsSecretsService : AwsSecretsService {
    override fun getAfsFeedbackEndpointCredentials() = UsernamePassword("afs", "afs123")
}
