package com.jet.service.adventure.service

import com.jet.service.adventure.ewi.model.CreateLoanApplicationParam
import com.jet.service.adventure.ewi.model.MarkFlagParam
import com.jet.service.adventure.ewi.service.EwiService
import com.jet.service.adventure.model.LoanApplicationDocumentAppendRequest
import com.jet.service.adventure.model.LoanApplicationDocumentAppendResponse
import com.jet.service.adventure.model.LoanApplicationRequest
import com.jet.service.adventure.model.LoanApplicationResponse
import com.jet.service.adventure.model.LoanUsage
import com.jet.service.adventure.model.Product
import com.jet.service.adventure.model.ProductType
import com.jet.service.adventure.util.toProductType
import org.springframework.stereotype.Service
import java.time.LocalDate
import java.time.LocalTime
import java.time.ZoneId
import java.time.ZonedDateTime
import java.time.chrono.ThaiBuddhistDate
import java.time.format.DateTimeFormatter

@Service
class LoanApplicationService(val ewiService: EwiService) {

    fun create(request: LoanApplicationRequest): LoanApplicationResponse {
        val result = ewiService.createLoanApplication(
                buildCreateLoanApplicationParam(request), getProductType(request.product))
        return LoanApplicationResponse(result.loanId, result.company, result.productCode.toProductType(), result.branchCode,
                result.jetNumber, result.regionCode, result.hubId)
    }

    fun setAppend(request: LoanApplicationDocumentAppendRequest): LoanApplicationDocumentAppendResponse {
        val markFlagParam = MarkFlagParam(
            "GECAL",
            request.productType.toProductCode(),
            request.branchCode, request.applicationNo,
            "A", "JT", request.jetId
        )
        ewiService.markFlag(markFlagParam, request.productType)
        return LoanApplicationDocumentAppendResponse(request.jetId)
    }

    fun afsDate(date: LocalDate): String {
        val thaiDate = ThaiBuddhistDate.from(date)
        val formatter = DateTimeFormatter.ofPattern("ddMMyyyy")
        return formatter.format(thaiDate)
    }

    fun afsTime(zonedDateTime: ZonedDateTime): String {
        val localTime = LocalTime.from(zonedDateTime.withZoneSameInstant( ZoneId.of( "Asia/Bangkok" )))
        val timeFormatter = DateTimeFormatter.ofPattern("HHmmss")
        return timeFormatter.format(localTime)
    }

    fun afsBool(value: Boolean): String {
        return when (value) {
            true -> "Y"
            false -> "N"
        }
    }

    fun formatLoanUsage(usage: LoanUsage): String {
        val answers = listOf(
            "VEHICLE:${afsBool(usage.vehicle)}",
            "RESIDENT:${afsBool(usage.resident)}",
            "CREDIT_CARD:${afsBool(usage.creditCard)}",
            "OVER_DRAFT:${afsBool(usage.overDraft)}",
            "PERSONAL_LOAN:${afsBool(usage.personalLoan)}"
        )
        return answers.joinToString(",")
    }

    private fun buildCreateLoanApplicationParam(request: LoanApplicationRequest): CreateLoanApplicationParam {
        return CreateLoanApplicationParam(
                thTitle = request.thTitle,
                thFirstName = request.thFirstName,
                thSurname = request.thSurname,
                enTitle = request.enTitle,
                enFirstName = request.enFirstName,
                enSurname = request.enSurname,
                birthDate = afsDate(request.birthDate),
                customerType = "",
                idCard = request.idCard,
                idCardExpire = afsDate(request.idCardExpire),
                cardType = "",
                marital = "",
                address1 = "",
                address2 = "",
                address3 = "",
                zipCode = request.zipCode,
                campaignCode = "",
                ncbType = "",
                income = request.income,
                product = request.product.afsValue,
                occupationCode = "",
                mobileNo = request.mobileNo,
                marketingDate = afsDate(LocalDate.from(request.marketingDateTime)),
                marketingTime = afsTime(request.marketingDateTime),
                branchCode = "",
                mrCode = "",
                dealerCode = "",
                telesalesCode = "",
                riskQuestion1 = "00%02d".format(request.yearOfService),
                riskQuestion2 = afsBool(request.applyForLoan),
                riskQuestion3 = formatLoanUsage(request.loanUsage),
                riskQuestion4 = afsBool(request.overdue),
                jetNumber = request.jetId,
                other = ""
        )
    }

    private fun getProductType(product: Product): ProductType {
        return when (product) {
            Product.NEW_CAR, Product.USED_CAR -> ProductType.FOUR_WHEELER
            Product.BIG_BIKE, Product.MOTORCYCLE -> ProductType.TWO_WHEELER
        }
    }
}