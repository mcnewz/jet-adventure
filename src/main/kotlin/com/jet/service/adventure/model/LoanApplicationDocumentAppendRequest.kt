package com.jet.service.adventure.model

data class LoanApplicationDocumentAppendRequest (
    val productType: ProductType,
    val branchCode: String,
    val applicationNo: String,
    val jetId: String
)