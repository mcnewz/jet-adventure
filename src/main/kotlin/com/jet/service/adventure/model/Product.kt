package com.jet.service.adventure.model

enum class Product(val afsValue: String) {
    NEW_CAR("New Car"),
    USED_CAR("Used Car"),
    BIG_BIKE("Big Bike"),
    MOTORCYCLE("Motorcycle")
}