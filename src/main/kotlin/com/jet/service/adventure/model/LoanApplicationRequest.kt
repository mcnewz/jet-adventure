package com.jet.service.adventure.model

import com.jet.service.adventure.util.MaskedField
import com.jet.service.adventure.util.dataClassToString
import java.time.LocalDate
import java.time.ZonedDateTime
import javax.validation.constraints.NotBlank

data class LoanUsage(
    val vehicle: Boolean,
    val resident: Boolean,
    val creditCard: Boolean,
    val overDraft: Boolean,
    val personalLoan: Boolean
)

data class LoanApplicationRequest (
    @get:NotBlank(message = "jetId must not be blank.")
val jetId: String,
    val thTitle: String,
    val thFirstName: String,
    val thSurname: String,
    val enTitle: String,
    val enFirstName: String,
    val enSurname: String,
    val birthDate: LocalDate,
    @MaskedField val idCard: String,
    val idCardExpire: LocalDate,
    val zipCode: String,
    val income: String,
    val product: Product,
    @MaskedField val mobileNo: String,
    val marketingDateTime: ZonedDateTime,
    val yearOfService: Int,
    val applyForLoan: Boolean,
    val loanUsage: LoanUsage,
    val overdue: Boolean
) {
    override fun toString(): String {
        return dataClassToString(this)
    }
}