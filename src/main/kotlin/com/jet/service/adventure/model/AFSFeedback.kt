package com.jet.service.adventure.model

import java.time.ZonedDateTime

data class AFSFeedback(val triggerCode: TriggerCode, val application: Application)

data class MarketRepresentative(
    val mobileNumber: String,
    val code: String,
    val name: String
)

data class Application(
    val loanId: String,
    val company: String,
    val productType: ProductType,
    val branch: String,
    val status: ApplicationStatus,
    val mr: MarketRepresentative,
    val verification: Verification,
    val statusDateTime: ZonedDateTime,
    val preApprove: PreApprove
)

data class Verification(
    val status: VerificationStatus,
    val reasonCode: String,
    val citizenIdStatus: Boolean,
    val paySlipStatus: Boolean,
    val statementOrEmployeeCardStatus: Boolean
)

data class PreApprove(val amount: Double?, val term: Int?, var percentDown: Double?, val installment: Double?)

enum class ApplicationStatus {
    ACTIVE,
    CONTRACTED,
    AUTO_CANCEL,
    NOT_ACTIVE
}

enum class VerificationStatus {
    PENDING,
    PRE_APPROVED,
    REJECT,
    MANUAL_CANCEL,
    AUTOMATIC_CANCEL,
    HARD_CASE,
    UTILIZE
}

enum class TriggerCode {
    UPDATE_INFORMATION,
    REJECT,
    PRE_APPROVED,
    HARD_CASE,
    CREDIT_LINE_EXPIRED,
    LOAN_UTILIZED,
    MANUAL_ASSIGN_MR,
    RETAKE_PHOTO,
    CANCEL,
    OTHERS;
}