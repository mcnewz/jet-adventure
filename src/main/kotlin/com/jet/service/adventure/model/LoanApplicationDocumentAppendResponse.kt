package com.jet.service.adventure.model

data class LoanApplicationDocumentAppendResponse(val jetId: String)