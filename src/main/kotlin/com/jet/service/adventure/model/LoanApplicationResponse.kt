package com.jet.service.adventure.model

data class LoanApplicationResponse (
    val loanApplicationID: String,
    val company: String,
    val productType: ProductType,
    val branchCode: String,
    val jetNumber: String,
    val regionCode: String,
    val hubId: String
)