package com.jet.service.adventure.model

import java.time.LocalDate

data class DocumentRequest(
    val documentSetId: String?,
    val applicationNo: String,
    val customerFirstName: String,
    val customerLastName: String,
    val customerBirthDate: LocalDate,
    val customerCitizenId: String,
    val branchId: String,
    val hubId: String,
    val productType: ProductType,
    val imageFilePaths: List<String>
)