package com.jet.service.adventure.model

enum class ProductType {
    FOUR_WHEELER,
    TWO_WHEELER;

    fun toProductCode(): String {
        return when (this) {
            FOUR_WHEELER -> "HP"
            TWO_WHEELER -> "MC"
        }
    }
}