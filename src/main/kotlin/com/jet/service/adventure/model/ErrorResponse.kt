package com.jet.service.adventure.model

data class ErrorResponse(val error: String, val code: String, val message: String)