package com.jet.service.adventure.exceptions

import com.jet.service.adventure.ewi.exception.AfsResponseException
import com.jet.service.adventure.ewi.exception.EwiResponseException
import com.jet.service.adventure.ewi.exception.EwiServiceValidationException
import com.jet.service.adventure.idoc.exception.IDocGenerateDocumentSetIdException
import com.jet.service.adventure.idoc.exception.IDocGetDocumentSetException
import com.jet.service.adventure.idoc.exception.IDocSubmitException
import com.jet.service.adventure.idoc.exception.IDocUpdateApplicationNoException
import com.jet.service.adventure.ifu.exception.IFuGetRepositoryException
import com.jet.service.adventure.ifu.exception.IFuUploadException
import com.jet.service.adventure.model.ErrorResponse
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.converter.HttpMessageNotReadableException
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestControllerAdvice
import org.springframework.web.context.request.WebRequest

@RestControllerAdvice
class GlobalExceptionHandler {
    val logger: Logger = LoggerFactory.getLogger(GlobalExceptionHandler::class.java)

    private fun optionalString(s: String?): String {
        return s ?: ""
    }

    private fun logAndReturn(error: String?, code: String, message: String?): ErrorResponse {
        val response = ErrorResponse(optionalString(error), code, optionalString(message))
        logger.error(response.toString())
        return response
    }

    @ExceptionHandler(AfsResponseException::class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    fun afsErrorHandler(e: AfsResponseException): ErrorResponse {
        return logAndReturn(e::class.simpleName, e.code, e.message)
    }

    @ExceptionHandler(EwiServiceValidationException::class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    fun ewiValidationErrorHandler(e: EwiServiceValidationException): ErrorResponse {
        return logAndReturn(e::class.simpleName, "", e.message)
    }

    @ExceptionHandler(EwiResponseException::class)
    @ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
    fun ewiErrorHandler(e: EwiResponseException): ErrorResponse {
        return logAndReturn(e::class.simpleName, e.code, e.message)
    }

    @ExceptionHandler(HttpMessageNotReadableException::class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    fun httpMessageNotReadableExceptionHandler(e: HttpMessageNotReadableException): ErrorResponse {
        return logAndReturn("Validation Error", "", e.message)
    }

    @ExceptionHandler(MethodArgumentNotValidException::class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    fun methodArgumentNotValidExceptionHandler(e: MethodArgumentNotValidException): ErrorResponse {
        return logAndReturn("Validation Error", "", e.message)
    }

    @ExceptionHandler(
            IFuUploadException::class,
            IDocGenerateDocumentSetIdException::class,
            IDocSubmitException::class,
            IDocGetDocumentSetException::class,
            IDocUpdateApplicationNoException::class)
    @ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
    fun serviceUnavailableExceptionHandler(e: RuntimeException): ErrorResponse {
        return logAndReturn(e::class.simpleName, "", e.message)
    }

    @ExceptionHandler(IFuGetRepositoryException::class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    fun iFuGetRepositoryExceptionHandler(e: IFuGetRepositoryException): ErrorResponse {
        return logAndReturn(e::class.simpleName, "", e.message)
    }

    @ExceptionHandler(Exception::class)
    fun handleAllExceptions(ex: Exception, request: WebRequest): ErrorResponse {
        logger.error("Unexpected error occurred", ex)
        return ErrorResponse(ex.toString(), "", ex.message!!)
    }
}