package com.jet.service.adventure.exceptions

class AfsFeedbackParsingException(message: String?) : RuntimeException(message)