package com.jet.service.adventure.ifu

import com.jet.service.adventure.biztech.Entity

interface IFuClient {
    fun upload(
        entity: Entity,
        fileName: String,
        content: String
    )
    fun getRepositoryPath(entity: Entity): String
}