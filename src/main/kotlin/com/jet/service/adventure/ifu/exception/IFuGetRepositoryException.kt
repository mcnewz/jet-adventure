package com.jet.service.adventure.ifu.exception

class IFuGetRepositoryException(message: String?) : RuntimeException(message)