package com.jet.service.adventure.ifu

import com.biztech.base.backend.service.exception.BaseBizTeCHServiceException
import com.biztech.ifu.service.FileUploadService
import com.biztech.ifu.service.RepositoryService
import com.biztech.ifu.vo.FileUploadTokenVO
import com.jet.service.adventure.biztech.Entity
import com.jet.service.adventure.ifu.exception.IFuGetRepositoryException
import com.jet.service.adventure.ifu.exception.IFuUploadException
import com.jet.service.adventure.ifu.model.EntityConfiguration
import com.jet.service.adventure.ifu.model.IFuConfigurationProperties
import com.jet.service.adventure.biztech.WebService
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Component
import java.util.UUID

@Component
@Profile("IFU_LIVE_ACCESS")
class IFuClientImpl(val config: IFuConfigurationProperties) : IFuClient {

    override fun upload(
        entity: Entity,
        fileName: String,
        content: String
    ) {
        val config = entityConfig(entity)
        val refId = UUID.randomUUID().toString().filter { it != '-' }
        val fileUploadService = WebService.build<FileUploadService>(
                config.endpoint, config.timeout)
        val fileUploadTokenVO = FileUploadTokenVO(fileName, -1, content, config.repositoryId, refId)
        try {
            fileUploadService.upload(fileUploadTokenVO)
        } catch (e: BaseBizTeCHServiceException) {
            throw IFuUploadException(e.message)
        }
    }

    override fun getRepositoryPath(entity: Entity): String {
        val config = entityConfig(entity)
        val repositoryService = WebService.build<RepositoryService>(
                config.endpoint, config.timeout)
        try {
            val repository = repositoryService.getRepository(config.repositoryId)
            return repository.path
        } catch (e: BaseBizTeCHServiceException) {
            throw IFuGetRepositoryException(e.message)
        }
    }

    private fun entityConfig(entity: Entity): EntityConfiguration {
        return when (entity) {
            Entity.BAY -> config.bay
            Entity.AYCAL -> config.aycal
        }
    }
}