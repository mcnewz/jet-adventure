package com.jet.service.adventure.ifu

import com.jet.service.adventure.biztech.Entity
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Component

@Component
@Profile("IFU_STUB_ACCESS")
class IFuClientStub : IFuClient {

    override fun upload(entity: Entity, fileName: String, content: String) {
    }

    override fun getRepositoryPath(entity: Entity): String {
        return "mock-file-path"
    }
}