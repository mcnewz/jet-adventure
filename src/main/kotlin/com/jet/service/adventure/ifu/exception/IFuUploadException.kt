package com.jet.service.adventure.ifu.exception

class IFuUploadException(message: String?) : RuntimeException(message)