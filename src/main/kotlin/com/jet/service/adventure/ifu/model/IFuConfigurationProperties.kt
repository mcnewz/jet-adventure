package com.jet.service.adventure.ifu.model

import org.springframework.boot.context.properties.ConfigurationProperties

data class EntityConfiguration(
    var endpoint: String = "",
    var repositoryId: String = "",
    var timeout: Int = 50000
)

@ConfigurationProperties("ifu")
data class IFuConfigurationProperties(
    var bay: EntityConfiguration = EntityConfiguration(),
    var aycal: EntityConfiguration = EntityConfiguration()
)