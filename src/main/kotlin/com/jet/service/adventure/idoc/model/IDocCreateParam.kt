package com.jet.service.adventure.idoc.model

import java.time.LocalDate

data class IDocCreateParam (
    val customerFirstName: String,
    val customerLastName: String,
    val customerBirthDate: LocalDate,
    val customerCitizenId: String,
    val applicationNo: String,
    val branchId: String,
    val hubId: String,
    val productCode: String,
    val imageFilePaths: List<String>
)