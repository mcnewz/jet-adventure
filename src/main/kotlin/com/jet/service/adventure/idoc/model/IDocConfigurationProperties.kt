package com.jet.service.adventure.idoc.model

import org.springframework.boot.context.properties.ConfigurationProperties

data class ServiceConfiguration(
    var endpoint: String = "",
    var timeout: Int = 50000
)

data class EntityConfiguration(
    var idoc: ServiceConfiguration = ServiceConfiguration(),
    var docprocess: ServiceConfiguration = ServiceConfiguration(),
    var monitorId: String = "",
    var documentProcessId: String = "",
    var documentMonitorObjIdPrefix: String = ""
)

@ConfigurationProperties("idoc")
data class IDocConfigurationProperties(
    var applicantType: String = "",
    var documentSourceChannel: String = "",
    var sender: String = "",
    var userAgent: String = "",
    var newOriginateMode: String = "",
    var appendOriginateMode: String = "",
    var mode: String = "",
    var bay: EntityConfiguration = EntityConfiguration(),
    var aycal: EntityConfiguration = EntityConfiguration()
)