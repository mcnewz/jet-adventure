package com.jet.service.adventure.idoc.exception

class IDocGetDocumentSetException(message: String?) : RuntimeException(message)