package com.jet.service.adventure.idoc

import com.biztech.base.backend.service.exception.BaseBizTeCHServiceException
import com.biztech.idociapps.docprocess.service.DocumentProcessService
import com.biztech.idociapps.document.service.CreditDocumentService
import com.biztech.idociapps.vo.CreditDocumentSetVO
import com.biztech.idocprocess.vo.DocumentProcessVO
import com.jet.service.adventure.biztech.Entity
import com.jet.service.adventure.biztech.WebService
import com.jet.service.adventure.idoc.exception.IDocGenerateDocumentSetIdException
import com.jet.service.adventure.idoc.exception.IDocGetDocumentSetException
import com.jet.service.adventure.idoc.exception.IDocSubmitException
import com.jet.service.adventure.idoc.exception.IDocUpdateApplicationNoException
import com.jet.service.adventure.idoc.model.EntityConfiguration
import com.jet.service.adventure.idoc.model.IDocConfigurationProperties
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Service

@Service
@Profile("IDOC_LIVE_ACCESS")
class IDocClientImpl(val config: IDocConfigurationProperties) : IDocClient {

    override fun execDocumentProcess(entity: Entity, documentProcessVO: DocumentProcessVO): String {
        val config = entityConfig(entity).docprocess
        val documentProcessService = WebService.build<DocumentProcessService>(config.endpoint, config.timeout)
        try {
            return documentProcessService.execute(documentProcessVO)
        } catch (e: Exception) {
            throw IDocSubmitException(e.message)
        }
    }

    override fun generateDocumentSetId(entity: Entity): String {
        val config = entityConfig(entity).idoc
        val creditDocumentService = WebService.build<CreditDocumentService>(config.endpoint, config.timeout)

        try {
            val documentSetId = creditDocumentService.genCreditDocumentSetId()
            return documentSetId
        } catch (e: BaseBizTeCHServiceException) {
            throw IDocGenerateDocumentSetIdException(e.message)
        }
    }

    override fun getDocumentSetByApplicationNo( entity: Entity, applicationNo: String): CreditDocumentSetVO? {
        val config = entityConfig(entity).idoc
        val creditDocumentService = WebService.build<CreditDocumentService>(config.endpoint, config.timeout)
        try {
            val documentSetVOs = creditDocumentService.listDocumentSetByApplicationNo(applicationNo)
            if (documentSetVOs != null && documentSetVOs.size > 0) {
                return documentSetVOs[0]
            }
            return null
        } catch (e: Throwable) {
            throw IDocGetDocumentSetException(e.message)
        }
    }

    override fun getDocumentSetById(entity: Entity, documentSetId: String): CreditDocumentSetVO? {
        val config = entityConfig(entity).idoc
        val creditDocumentService = WebService.build<CreditDocumentService>(config.endpoint, config.timeout)
        try {
            return creditDocumentService.getDocumentSet(documentSetId)
        } catch (e: Throwable) {
            throw IDocGetDocumentSetException(e.message)
        }
    }

    override fun updateDocumentSet(entity: Entity, documentSet: CreditDocumentSetVO) {
        val config = entityConfig(entity).idoc
        val creditDocumentService = WebService.build<CreditDocumentService>(config.endpoint, config.timeout)
        try {
            creditDocumentService.updateDocumentSet(documentSet)
        } catch (e: Throwable) {
            throw IDocUpdateApplicationNoException(e.message)
        }
    }

    private fun entityConfig(entity: Entity): EntityConfiguration {
        return when (entity) {
            Entity.BAY -> config.bay
            Entity.AYCAL -> config.aycal
        }
    }
}