package com.jet.service.adventure.idoc

import com.biztech.idociapps.vo.CreditDocumentSetVO
import com.biztech.idocprocess.vo.DocumentProcessVO
import com.jet.service.adventure.biztech.Entity
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Service

@Service
@Profile("IDOC_STUB_ACCESS")
class IDocClientStub : IDocClient {
    override fun updateDocumentSet(entity: Entity, documentSet: CreditDocumentSetVO) {
    }

    private val mockDocumentSetId = "some-document-set-id"

    override fun generateDocumentSetId(entity: Entity): String = mockDocumentSetId

    override fun execDocumentProcess(entity: Entity, documentProcessVO: DocumentProcessVO): String = mockDocumentSetId

    override fun getDocumentSetByApplicationNo(entity: Entity, applicationNo: String): CreditDocumentSetVO? =
        mockCreditDocumentSetVO()

    override fun getDocumentSetById(entity: Entity, documentSetId: String): CreditDocumentSetVO? =
        mockCreditDocumentSetVO()

    private fun mockCreditDocumentSetVO(): CreditDocumentSetVO {
        val creditDocumentSetVO = CreditDocumentSetVO()
        creditDocumentSetVO.id = "183949585875"
        return creditDocumentSetVO
    }
}