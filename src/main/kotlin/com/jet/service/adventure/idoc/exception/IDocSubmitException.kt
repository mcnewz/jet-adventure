package com.jet.service.adventure.idoc.exception

class IDocSubmitException(message: String?) : RuntimeException(message)