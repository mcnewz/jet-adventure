package com.jet.service.adventure.idoc.model

import java.time.LocalDate

data class IDocCustomerInfo(
    val firstName: String,
    val lastName: String,
    val birthDate: LocalDate,
    val citizenId: String
)