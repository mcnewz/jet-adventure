package com.jet.service.adventure.idoc

import com.biztech.idociapps.vo.CreditDocumentSetVO
import com.biztech.idocprocess.vo.DocumentProcessVO
import com.jet.service.adventure.biztech.Entity

interface IDocClient {

    fun execDocumentProcess(entity: Entity, documentProcessVO: DocumentProcessVO): String

    fun generateDocumentSetId(entity: Entity): String

    fun getDocumentSetByApplicationNo(entity: Entity, applicationNo: String): CreditDocumentSetVO?

    fun getDocumentSetById(entity: Entity, documentSetId: String): CreditDocumentSetVO?

    fun updateDocumentSet(entity: Entity, documentSet: CreditDocumentSetVO)
}