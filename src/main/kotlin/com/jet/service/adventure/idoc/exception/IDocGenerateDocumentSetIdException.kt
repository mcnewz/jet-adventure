package com.jet.service.adventure.idoc.exception

class IDocGenerateDocumentSetIdException(message: String?) : RuntimeException(message)