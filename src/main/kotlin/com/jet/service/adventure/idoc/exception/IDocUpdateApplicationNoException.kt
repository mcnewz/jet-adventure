package com.jet.service.adventure.idoc.exception

class IDocUpdateApplicationNoException(message: String?) : RuntimeException(message)