package com.jet.service.adventure.idoc

import com.biztech.idociapps.vo.CreditDocumentSetVO
import com.biztech.idocprocess.vo.DocumentProcessDetailVO
import com.biztech.idocprocess.vo.DocumentProcessVO
import com.jet.service.adventure.biztech.Entity
import com.jet.service.adventure.idoc.model.EntityConfiguration
import com.jet.service.adventure.idoc.model.IDocAppendParam
import com.jet.service.adventure.idoc.model.IDocConfigurationProperties
import com.jet.service.adventure.idoc.model.IDocCreateParam
import com.jet.service.adventure.idoc.model.IDocCustomerInfo
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.time.LocalDate
import java.time.ZoneId
import java.util.Date

@Service
class IDocService(val iDocClient: IDocClient, val config: IDocConfigurationProperties) {

    val logger: Logger = LoggerFactory.getLogger(this::class.java)

    fun create(param: IDocCreateParam): String {
        logger.info("Request : $param")
        val entity = Entity.fromProductCode(param.productCode)
        logger.info("Generating documentSetId...")
        val documentSetId = iDocClient.generateDocumentSetId(entity)
        logger.info("Generate documentSetId done. $documentSetId")
        val documentProcessVO = buildCreateDocumentProcessVO(entity, documentSetId, param)
        logger.info("Executing Create document process... : $param")
        iDocClient.execDocumentProcess(entity, documentProcessVO)
        logger.info("Executing Create document process done.")
        return documentSetId
    }

    fun append(param: IDocAppendParam): String {
        logger.info("Request : $param")
        val entity = Entity.fromProductCode(param.productCode)
        val documentProcessVO = buildAppendDocumentProcessVO(entity, param)
        logger.info("Executing Append document process... : $param")
        iDocClient.execDocumentProcess(entity, documentProcessVO)
        logger.info("Executing Append document process done.")
        return param.documentSetId
    }

    fun getDocumentSetIdByApplicationNo(productCode: String, applicationNo: String): String? {
        val entity = Entity.fromProductCode(productCode)
        val creditDocumentSetVO = iDocClient.getDocumentSetByApplicationNo(entity, applicationNo)
        return creditDocumentSetVO?.id
    }

    fun getDocumentSetById(productCode: String, documentSetId: String): CreditDocumentSetVO? {
        val entity = Entity.fromProductCode(productCode)
        return iDocClient.getDocumentSetById(entity, documentSetId)
    }

    fun unResetData(productCode: String, documentSet: CreditDocumentSetVO, applicationNo: String, customerInfo: IDocCustomerInfo) {
        val entity = Entity.fromProductCode(productCode)
        documentSet.applicationNo = applicationNo
        documentSet.productType = productCode
        documentSet.thaiFirstName = customerInfo.firstName
        documentSet.thaiLastName = customerInfo.lastName
        documentSet.dateOfBirth = asDate(customerInfo.birthDate)
        documentSet.customerId = customerInfo.citizenId
        documentSet.applicantType = config.applicantType
        documentSet.action = "SAVE_APP_NO"
        iDocClient.updateDocumentSet(entity, documentSet)
    }

    private fun buildCreateDocumentProcessVO(entity: Entity, documentSetId: String, param: IDocCreateParam): DocumentProcessVO {
        val entityConfig = entityConfig(entity)
        val documentProcessVO = DocumentProcessVO()
        documentProcessVO.documentSetId = documentSetId
        documentProcessVO.mode = config.mode
        documentProcessVO.customerFirstName = param.customerFirstName
        documentProcessVO.customerLastName = param.customerLastName
        documentProcessVO.applicationNo = param.branchId + param.applicationNo
        documentProcessVO.branchId = param.branchId
        documentProcessVO.hubId = param.hubId
        documentProcessVO.applicantType = config.applicantType
        documentProcessVO.productCode = param.productCode
        documentProcessVO.dob = asDate(param.customerBirthDate)
        documentProcessVO.citizenId = param.customerCitizenId
        documentProcessVO.documentSourceChannel = config.documentSourceChannel
        documentProcessVO.sender = config.sender
        documentProcessVO.userAgent = config.userAgent
        documentProcessVO.originateMode = config.newOriginateMode
        documentProcessVO.sendDate = Date()
        documentProcessVO.documentMonitorObjId = "${entityConfig.documentMonitorObjIdPrefix}${param.hubId}"
        documentProcessVO.documentProcessId = entityConfig.documentProcessId
        documentProcessVO.monitorId = entityConfig.monitorId

        documentProcessVO.documentProcessDetailVOs = buildImageDetail(param.imageFilePaths)
        return documentProcessVO
    }

    private fun buildAppendDocumentProcessVO(entity: Entity, param: IDocAppendParam): DocumentProcessVO {
        val entityConfig = entityConfig(entity)
        val documentProcessVO = DocumentProcessVO()
        documentProcessVO.hubId = param.hubId
        documentProcessVO.branchId = param.branchId
        documentProcessVO.sender = config.sender
        documentProcessVO.mode = config.mode
        documentProcessVO.appendDocumentSetId = param.documentSetId
        documentProcessVO.appendProductCode = param.productCode
        documentProcessVO.originateMode = config.appendOriginateMode
        documentProcessVO.documentMonitorObjId = "${entityConfig.documentMonitorObjIdPrefix}${param.hubId}"
        documentProcessVO.documentProcessId = entityConfig.documentProcessId
        documentProcessVO.monitorId = entityConfig.monitorId

        documentProcessVO.documentProcessDetailVOs = buildImageDetail(param.imageFilePaths)
        return documentProcessVO
    }

    private fun asDate(localDate: LocalDate): Date {
        return Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant())
    }

    private fun buildImageDetail(imageFilePaths: List<String>): ArrayList<DocumentProcessDetailVO> {
        val details = arrayListOf<DocumentProcessDetailVO>()
        for (imageFilePath in imageFilePaths) {
            val detail = DocumentProcessDetailVO()
            detail.documentType = "PHOTO_JOTHER"
            detail.sourcePath = imageFilePath
            details.add(detail)
        }
        return details
    }

    private fun entityConfig(entity: Entity): EntityConfiguration {
        return when (entity) {
            Entity.BAY -> config.bay
            Entity.AYCAL -> config.aycal
        }
    }
}