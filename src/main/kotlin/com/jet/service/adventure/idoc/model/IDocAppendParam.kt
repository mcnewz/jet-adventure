package com.jet.service.adventure.idoc.model

data class IDocAppendParam(
    val documentSetId: String,
    val productCode: String,
    val hubId: String,
    val branchId: String,
    val imageFilePaths: List<String>
)