package com.jet.service.adventure.actuator

import com.jet.service.adventure.ewi.model.EwiConfigurationProperties
import com.mashape.unirest.http.Unirest
import org.apache.http.conn.ssl.NoopHostnameVerifier
import org.apache.http.impl.client.HttpClients
import org.apache.http.ssl.SSLContextBuilder
import org.springframework.boot.actuate.health.Health
import org.springframework.boot.actuate.health.HealthIndicator
import org.springframework.stereotype.Component

@Component
class EwiHealth(val ewiConfig: EwiConfigurationProperties) : HealthIndicator {

    private val client = HttpClients.custom()
        .setSSLContext(SSLContextBuilder().loadTrustMaterial(null) { _, _ -> true }.build())
        .setSSLHostnameVerifier(NoopHostnameVerifier())
        .build()

    override fun health(): Health {
        try {
            Unirest.setHttpClient(client)
            val response = Unirest.get("${ewiConfig.endpoint}?wsdl").asString()
            if (response.status == 200) {
                return Health.up().build()
            }
            return Health.down().withDetail(response.statusText, response.status).build()
        } catch (thw: Throwable) {
            return Health.down().withDetail(thw.message, 0).build()
        }
    }
}